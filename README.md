
# Setup dev environment
## Create dev and test DBs
```
CREATE DATABASE REPLACE_with_db_name;
CREATE DATABASE REPLACE_with_test_db_name;
```

## Create User
```
GRANT ALL PRIVILEGES on REPLACE_with_db_name.* to 'REPLACE_with_db_user'@'localhost' IDENTIFIED BY 'REPLACE_with_db_pass';
GRANT ALL PRIVILEGES on REPLACE_with_test_db_name.* to 'REPLACE_with_db_user'@'localhost' IDENTIFIED BY 'REPLACE_with_db_pass';
```

## Set your database url and credentials
Use the following command to copy the sample config file to your home directory.
If it already exists, the command will just append the content of the file.

```
cat src/main/resources/spring-boot-devtools.properties.sample >> ~/.spring-boot-devtools.properties

```

```
cp src/test/java/fr/univ/amu/pacome/test/data/testDBUnit-context.xml.sample src/test/java/fr/univ/amu/pacome/test/data/testDBUnit-context.xml
```

Then edit the values to match your database.

# Set your test database credentials 
```
Create copy of testBDUnit-context.xml in package fr.univ.amu.pacome.test.data
set your credentials in this configuration file
```

# Download the front-end dependencies
## Install NPM (comes with Node.js)
You shoud be able to the `npm --version` command.
## Run npm
```
cd src/main/resources/static; npm install; cd -
```

# Run the tests

```
mvn test
```

# Insert example data in database
```
Run the test class named GenerData.java in package fr.univ.amu.pacome
```

# Run the app from the command line
```
mvn spring-boot:run
```
# Usual problems when importing project in eclipse
- JPA is not installed: Go to your project properties, then type facet in search field. Click on project facets then tick the JPA box in the right hand field and select its version in 2.0
- Class are not referenced in persistence.XML: go to JPA in your project properties, then in the right hand field go to "persistent class management" fieldset and select "discover annotated classes automatically"

# License
GNU AGPLv3+ (+ means "or any later version")
