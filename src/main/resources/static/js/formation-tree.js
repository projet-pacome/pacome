"use strict";
var dataView;
var grid;
var data = [];
var nodeNameFormatter = nodeNameFormatterFactory();
var columns = [
	{id: "name", name: "Nom", field: "name", width: 600, formatter: nodeNameFormatter},
	{id: "code", name: "Code", field: "code", width: 170},
	{id: "type", name: "Type", field: "type"},
	{id: "credits", name: "Crédits", field: "credits", cssClass: "formatNumberCells"},
	{id: "hetd", name: "HETD", field: "hetdcomputed", cssClass: "formatNumberCells"},
	{id: "nbGroupsCM", name: "Groupes CM/TD/TP", width: 180, field: "nbGroups", cssClass: "formatNumberCells"}
];

var options = {
	editable: false,
	enableAddRow: false,
	enableCellNavigation: true,
	asyncEditorLoading: false
};

function getTreeFromServer(onSuccess) {
	var request = $.ajax({
		url: "./tree.json",
		method: "GET"
	});

	request.done(onSuccess);

	request.fail((jqXHR, textStatus) => {
		console.error(jqXHR);
		alert("Request failed: " + textStatus); // adoucir le message d'erreur
	});
}

function displayTree(tree) {
	data = prepareDataForSlickGrid(tree);
	createGridAndLoadData(data);
}

function prepareDataForSlickGrid(tree) {
	return deepFirstSearchToPrepareData({tree: tree, resultData: [], indentLevel: 0});
}

function deepFirstSearchToPrepareData({tree, resultData, indentLevel}={}) {
	var children = tree.children
	var currentNode = keepOnlyRootFromTree(tree);
	currentNode.indent = indentLevel;
	currentNode.id = currentNode.idNode; // SlickGrid mandates that each element implements a unique id property
	currentNode.nbGroups = concatGroups(currentNode);
	currentNode.hetdcomputed = replaceZeroByEmptyStringHetd(currentNode);
	currentNode.credits = replaceZeroByEmptyStringCredits(currentNode);
	resultData.push(prune(currentNode));
	children.forEach((child)=>{
		child.parent = tree; // don't use currentNode, it has a different value in many cases
		// TODO investigate why because not understanding this might lead to similar issues
		deepFirstSearchToPrepareData({tree: child, resultData: resultData, indentLevel: indentLevel+1});
	});
	return resultData;
}

function keepOnlyRootFromTree(tree) {
	delete tree.children;
	return tree;
}

function concatGroups(currentNode) {
	if(currentNode.nbGroupsCM == 0 || currentNode.nbGroupsCM == null)
		return "";
	return currentNode.nbGroupsCM + " / " + currentNode.nbGroupsTD + " / " + currentNode.nbGroupsTP;
}

function replaceZeroByEmptyStringHetd(currentNode) {
	if(currentNode.hetdcomputed == 0)
		return "";
	//currentNode.hetdcomputed = Math.ceil(currentNode.hetdcomputed);
	return currentNode.hetdcomputed;
}

function replaceZeroByEmptyStringCredits(currentNode) {
	if(currentNode.credits == 0)
		return "";
	return currentNode.credits;
}

function prune(node) {
	delete node.visited;
	delete node.sites;
	delete node.idNode;
	return node;
}

function createGridAndLoadData(data) {
	// initialize the model
	dataView = new Slick.Data.DataView({ inlineFilters: true });
	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.setFilter(myFilter);
	dataView.endUpdate();

	// initialize the grid
	grid = new Slick.Grid("#myGrid", dataView, columns, options);

	// set handler to collapse/expand children
	grid.onClick.subscribe(function (e, args) {
		if ($(e.target).hasClass("toggle")) {
			var item = dataView.getItem(args.row);
			if (item) {
				if (!item._collapsed) {
					item._collapsed = true;
				} else {
					item._collapsed = false;
				}
				dataView.updateItem(item.id, item);
			}
			e.stopImmediatePropagation();
		}
	});

	bindContextMenuHandlers();

	// wire up model events to drive the grid
	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.render();
	});

	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.render();
	});

	// wire up the search textbox to apply the filter to the model
	$("#txtSearch").keyup(function (e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		searchString = this.value;
		dataView.refresh();
	})
}

/*
 * All context menu related code is base on:
 * https://6pac.github.io/SlickGrid/examples/example7-events.html
 * https://github.com/6pac/SlickGrid/blob/master/examples/example7-events.html
 */
function bindContextMenuHandlers() {
	// right click on a row
	grid.onContextMenu.subscribe(function (e) {
		e.preventDefault();
		var cell = grid.getCellFromEvent(e);
		$("#contextMenu")
			.data("row", cell.row)
			.css("top", e.pageY)
			.css("left", e.pageX)
			.show();
		$("body").one("click", function () {
			$("#contextMenu").hide();
		});
	});

	// click on an item of the context menu
	$("#contextMenu").click(function (e) {
		if (!$(e.target).is("li")) {
			return;
		}
		var row = $(this).data("row");
		var action = $(e.target).attr("data");
		if (action === "createNewChild") {
			startChildCreation(data[row]);
		} else if (action === "editNode") {
			editNode(data[row]);
		} else if (action === "deleteNode") {
			if(data[row].type != "FRM") {
				deleteNode(data[row]);
			}else{
				alert("Imposible de supprimer un noeud de type FRM");
			}
		}
	});
}

function startChildCreation(parent) {
	var formationId = getFormationId();
	window.location.href = "/formations/" + formationId + "/nodes/" + parent.id + "/new-child";
}
function editNode(node) {
	var formationId = getFormationId();
	window.location.href = "/formations/" + formationId + "/nodes/" + node.id + "/edit";
}

function deleteNode(node) {
	var formationId = getFormationId();
	var parentId = node.parent.id
	window.location.href = "/formations/" + formationId + "/nodes/" + parentId + "/delete/"+ node.id;
}

function getFormationId() {
	var url = window.location.href;
	const regex = /.*formations\/(\d*)\/tree/g;
	return regex.exec(url)[1];
}

function nodeNameFormatterFactory() {
	return function (row, cell, value, columnDef, dataContext) {
		value = value.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
		var spacer = "<span style='display:inline-block;height:1px;width:" + (15 * dataContext["indent"]) + "px'></span>";
		var idx = dataView.getIdxById(dataContext.id);
		if (data[idx + 1] && data[idx + 1].indent > data[idx].indent) {
			if (dataContext._collapsed) {
				return spacer + " <span class='toggle expand'></span>&nbsp;" + value;
			} else {
				return spacer + " <span class='toggle collapse'></span>&nbsp;" + value;
			}
		} else {
			return spacer + " <span class='toggle'></span>&nbsp;" + value;
		}
	};
}

var searchString = "";
function myFilter(item) {
	if (searchString != "" && item["name"].indexOf(searchString) == -1) {
		return false;
	}
	if (item.parent != null) {
		var parent = item.parent;
		while (parent) {
			if (parent._collapsed || (searchString != "" && parent["name"].indexOf(searchString) == -1)) {
				return false;
			}
			parent = parent.parent;
		}
	}
	return true;
}

$(function () {
	getTreeFromServer(displayTree);
})