<#macro baseLayout page_title>
<!DOCTYPE HTML>
<html>
	<head>
   		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>Neo Pacome - ${page_title}</title>
    	<link href="/css/style.css" rel="stylesheet" />
    	<link href="/css/bootstrap.css" rel="stylesheet" />
    	<!--[if lt IE 9]>
    	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    	<![endif]-->
	</head>
	<body>
    	<#include "header.ftl">
    	<#include "menu.ftl">
    	<section aria-labelledby="sectionHeading">
    		<#nested/>
    	</section>
    	<#include "footer.ftl">
	</body>
</html>
</#macro>