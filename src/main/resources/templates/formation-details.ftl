<#ftl output_format="XHTML">
<#import "layout/layout.ftl" as layout>
<@layout.baseLayout (formation.name)!"Non renseigné">
<h1 id="sectionHeading">Détails de la formation</h1>
<article itemscope>
	<h2 itemprop="name">${(formation.name)!"Non renseigné"}</h2>
	<#--**********************-->
	<#--***** ID + Status**** -->
	<#--********************* -->
	<div class="menu-bar">
	<form id="return"><input type="button" onclick="location.href='/formations'" type="button" value="Retour"></input></form>
	<a title="Visionner l'arbre" href="/formations/${formation.idFormation}/tree"><span class="glyphicon glyphicon-tree-conifer tree-icon"></span></a>
	<a title="Calcul HETD" href="/formations/${formation.idFormation}/computeHETD"><span class="glyphicon glyphicon glyphicon-stats HETD-icon"></span></a>
	<a title="Validation" href="/formations/${formation.idFormation}/validate"><span class="glyphicon glyphicon-ok-sign valid-icon"></span></a>
	<a title="Edition" href="/formations/${formation.idFormation}/edit"><span class="glyphicon glyphicon-pencil edit-icon"></span></a>
	</div>
	<div id="id-status">
		<#--**********************-->
		<#--********* ID ******** -->
		<#--********************* -->
		<div id="id" class="details">
			<table class="vertical">
				<tr>
					<td>Composante</td><td>${formation.composante.name}</td>
				</tr>
				<tr>
					<td>Code</td><td>${formation.code}</td>
				</tr>
				<tr>
					<td>Nature</td><td>${formation.nature.name}</td>
				</tr>
				<tr>
					<td>Nom</td><td>${formation.name}</td>
				</tr>
				<tr>
					<td>Dernier HETD calculé</td><td><#if formation.root.HETDComputed??> ${formation.root.HETDComputed!!} </#if></td>
				</tr>
				<tr>
					<td>Nb crédits / heure</td><td>${formation.numberOfHoursPerCredit}</td>
				</tr>
			</table>
			<#--
			<span>Seuil : ${(formation.seuil)}</span>
			-->
		</div>
		<#--**********************-->
		<#--*** Status + tabs *** -->
		<#--********************* -->	
		<div id="status-tabs" class="details">
			<table class="vertical">
				<tr>
					<td>État</td><td><#if !formation.activationState>Non activée<#else>Activé</#if></td>
				</tr>
				<tr>
					<td>Statut</td><td><#if formation.validationState == "VALID">
					<span class="glyphicon glyphicon-ok-sign ok-icon" title="Formation valide"></span>
				<#elseif formation.validationState == "INVALID">
					<span class="glyphicon glyphicon-exclamation-sign exclamation-icon" title="Formation invalide"></span>
				<#else>
					<span class="glyphicon glyphicon-question-sign question-icon" title="Statut de la formation inconnue"></span>
				</#if></td>
				</tr>
			</table>
			<table>
				 <caption>Taille maximum des groupes</caption>
				 <thead>
					<tr itemprop="title">
						<td>Grp CM</td>
						<td>Grp TD</td>
						<td>Grp TP</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td itemprop="CM Group Size">${(formation.CMGroupSize)!"Non renseignée"}</td>
						<td itemprop="TD Group Size">${(formation.TDGroupSize)!"Non renseignée"}</td>
						<td itemprop="TP Group Size">${(formation.TPGroupSize)!"Non renseignée"}</td>
					</tr>
				</tbody>
			</table>
			<table>
				<caption>Pourcentage d'une formation</caption>
				<thead>
					<tr>	
						<td>Répartition des CM</td>
						<td>Répartition des TD</td>
						<td>Répartition des TP</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td itemprop="percent Of CM">${(formation.percentOfCM)!"Non renseigné"}%</td>
						<td itemprop="percent Of TD">${(formation.percentOfTD)!"Non renseigné"}%</td>
						<td itemprop="percent Of TP">${(formation.percentOfTP)!"Non renseigné"}%</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<#--**********************-->
	<#--*** Resp + Corres *** -->
	<#--********************* -->	
	<div id="responsables-correspondants">
		<table>
				<caption>Les correspondants de composante sont : </caption>
				<thead>
					<tr itemprop="title">
						<td>Nom</td>
						<td>Prénom</td>
					</tr>
				</thead>
				<tbody>
					<#list formation.composante.users >
						<#items as user>
							<tr>
								<td>${user.firstName}</td>
								<td>${user.lastName}</td>
							</tr>
						</#items>
					</#list>
				</tbody>
		</table>
		<#if formation.users?has_content>
			<table>
				<caption>Les responsables de cette formation sont : </caption>
				<thead>
					<tr itemprop="title">
						<td>Nom</td>
						<td>Prénom</td>
					</tr>
				</thead>
				<tbody>
					<#list formation.users>
						<#items as user>
							<tr>
								<td>${user.firstName}</td>
								<td>${user.lastName}</td>
							</tr>
						</#items>
					</#list>
				</tbody>
			</table>
		</#if>
	</div>
</article>
</@layout.baseLayout>