<#ftl output_format="XHTML">
<#import "layout/layout.ftl" as layout>
<@layout.baseLayout "Formations">
<h1 id ="sectionHeading">Liste des Formations</h1>

<#global total=0>

<#list formations>
<table>
	<thead>
		<tr>
			<td>Composante</td>
			<td>Code</td>
			<td>Nature</td>
			<td>Nom</td>
			<td>État</td>
			<td>Statut</td>
			<td>HETD</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
	<#items as formation >
		<#if formation.activationState>
		<tr>
			<td>${formation.composante.name}</td>
			<td>${formation.code}</td>
			<td>${(formation.nature.name)! "Non renseignée"}</td>
			<td>${formation.name}</td>
			<td>
				<#if !formation.activationState>
					<a title="Activer" href="/formations/${formation.idFormation}/toggle-state"><img class="off_button" src="img/off_button.png" alt="Off"></a>
				<#else>
					<a title="Desactiver" href="/formations/${formation.idFormation}/toggle-state"><img class="on_button" src="img/on_button.png" alt="On"></a>
				</#if>
			</td>
			<td>
				<#if formation.validationState == "VALID">
					<span class="glyphicon glyphicon-ok-sign ok-icon" title="Formation valide"></span>
				<#elseif formation.validationState == "INVALID">
					<span class="glyphicon glyphicon-exclamation-sign exclamation-icon" title="Formation invalide"></span>
				<#else>
					<span class="glyphicon glyphicon-question-sign question-icon" title="Statut de la formation inconnue"></span>
				</#if>
			</td>
			<td><#if formation.root.HETDComputed??>
					${formation.root.HETDComputed!!}
				</#if>
			</td>
			<#assign total = total + formation.root.HETDComputed!0/>
			<td><a title="Détails" href="/formations/${formation.idFormation}"><span class="glyphicon glyphicon-zoom-in details-icon"></span></a><a title="Visionner l'arbre" href="/formations/${formation.idFormation}/tree"><span class="glyphicon glyphicon-tree-conifer
 tree-icon"></span></a></td>
		</tr>
		</#if>
	</#items>
	</tbody>
	 <tfoot>
		 <tr>
			 <td colspan="8"> 
			 	Total des HETDs : <span>${total}</span>
			 </td> 
		 </tr>
	 </tfoot> 
</table>
<#else>
	<p>Vous n'êtes responsable d'aucune formation</p>
</#list>
</@layout.baseLayout>