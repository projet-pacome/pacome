<#ftl output_format="XHTML">
<#import "layout/layout.ftl" as layout>
<#import "spring.ftl" as spring />
<@layout.baseLayout "Edition">
<h1 id="sectionHeading">${(element.name)! "Nouvel élément"}</h1>
<form name="element" method="post">
	<fieldset>
		<legend><h3>Identifiants</h3></legend>
		<div class="fieldWrapper">
			<label for="code">Code:</label>
			<div class="input">
				<#--<@spring.bind "element.code" />-->
				<input id="code" type="text" name="code" required value="${(element.code)! ""}"/>
			</div>
			<div>
				<#--<@spring.showErrors separator=",<br> " classOrStyle="error"/>-->
				<#if duplicateCode??>
					<#--<@spring.messageText error, "Le code que vous avez choisie existe déjà"/>-->
				</#if>
			</div>
		</div>
		<div class="fieldWrapper">
			<label for="name">Nom:</label>
			<div class="input">
				<input id="name" type="text" name="name" required value="${(element.name)! ""}"/>
			</div>
		</div>
		<div class="fieldWrapper">
		<label for="type">Type:</label>
			<div class="input">
				<select id="type" name="type" required>
					<option value="">Aucun</option>
					<#list types>
						<#items as type>
							<#if (element.type)??>
								<#if element.type.name() == type.name()>
									<option value = "${type}" selected>${type.name()}</option>
								<#else>
									<option value = "${type}">${type.name()}</option>
								</#if>
							<#else>
								<option value = "${type}">${type.name()}</option>
							</#if>
						</#items>
					</#list>
				</select>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend><h3>Paramètres</h3></legend>
		<#setting locale="en_US">
		<div class="fieldWrapper">
			<label for="credits">Nombre de crédits:</label><div class="input"><input id="credits" type="number" name="credits" min="0" step="0.01" value="${(element.credits)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="HCM">Heures de CM:</label><div class="input"><input id="HCM" type="number" name="HCM" min="0" value="${(element.HCM)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="HTD">Heures de TD:</label><div class="input"><input id="HTD" type="number" name="HTD" min="0" value="${(element.HTD)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="HTP">Heures de TP:</label><div class="input"><input id="HTP" type="number" name="HTP" min="0" value="${(element.HTP)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="HNP">Heures de NP:</label><div class="input"><input id="HNP" type="number" name="HNP" min="0" value="${(element.HNP)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="HETD">Heures équivalent TD:</label><div class="input"><input id="HETD" type="number" name="HETD" min="0" value="${(element.HETD)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="CMGroupSize">Taille des groupes de CM:</label><div class="input"><input id="CMGroupSize" type="number" name="nbGroupsCM" min="0" value="${(element.nbGroupsCM)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="TDGroupSize">Taille des groupes de TD:</label><div class="input"><input id="TDGroupSize" type="number" name="nbGroupsTD" min="0" value="${(element.nbGroupsTD)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="TPGroupSize">Taille des groupes de TP:</label><div class="input"><input id="TPGroupSize" type="number" name="nbGroupsTP" min="0" value="${(element.nbGroupsTP)! ""}"/></div>
		</div>
	</fieldset>
	<input id="submit" type="submit" value="Sauvegarder"/>
	<input Type="button" value="Retour" onClick="history.go(-1);return true;">
</form>
</@layout.baseLayout>