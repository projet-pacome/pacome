<#ftl output_format="XHTML">
<#import "layout/layout.ftl" as layout>
<#import "spring.ftl" as spring />
<@layout.baseLayout "Arbre de formation">

<link rel="stylesheet" href="/node_modules/slickgrid/slick.grid.css" type="text/css"/>
<link rel="stylesheet" href="/node_modules/slickgrid/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>

	<style>
	@import url('/node_modules/slickgrid/slick-default-theme.css');
		.cell-title {
			font-weight: bold;
		}
		.cell-effort-driven {
			text-align: center;
		}
		.toggle {
			height: 9px;
			width: 9px;
			display: inline-block;
		}
		.toggle.expand {
			background: url(/node_modules/slickgrid/images/expand.gif) no-repeat center center;
		}
		.toggle.collapse {
			background: url(/node_modules/slickgrid/images/collapse.gif) no-repeat center center;
		}
		
		#contextMenu {
			width: 150px;
			z-index: 99999;
			font-size: 12px;
		}
		#contextMenu li {
			width: 100%;
			padding: 4px 14px 4px 14px;
			text-align: left;
			cursor: pointer;
		}
	</style>

<h1 id ="sectionHeading">Arbre de formation</h1>
<div class="menu-bar">
	<form id="return"><input type="button" onclick="location.href='/formations'" type="button" value="Retour"></input></form>
	<a title="Détails" href="/formations/${formation.idFormation}"><span class="glyphicon glyphicon-zoom-in details-icon"></span></a>
	</div>
<div id="tree">
	<div id="myGrid"></div>
</div>

<ul id="contextMenu" style="display:none;position:absolute">
	<li data="createNewChild">Créer élément enfant</li>
	<li data="editNode">Éditer élément</li>
	<li data="deleteNode">Supprimer élément</li>
</ul>
<script src="/node_modules/slickgrid/lib/jquery-1.11.2.min.js"></script>
<script src="/node_modules/slickgrid/lib/jquery-ui-1.11.3.min.js"></script>
<script src="/node_modules/slickgrid/lib/jquery.event.drag-2.3.0.js"></script>

<script src="/node_modules/slickgrid/slick.core.js"></script>
<#-- <script src="/node_modules/slickgrid/slick.formatters.js"></script>-->
<script src="/node_modules/slickgrid/slick.editors.js"></script>
<script src="/node_modules/slickgrid/slick.grid.js"></script>
<script src="/node_modules/slickgrid/slick.dataview.js"></script>

<script src="/js/formation-tree.js"></script>

</@layout.baseLayout>