<#ftl output_format="XHTML">
<#import "layout/layout.ftl" as layout>
<#import "spring.ftl" as spring />
<@layout.baseLayout "Edition">
<h1 id="sectionHeading">${(formation.name)! "Nouvelle formation"}</h1>
<form name="formation" action="/formations/save" method="post">
	<fieldset>
		<legend><h3>Identifiants</h3></legend>
		<div class="fieldWrapper">
			<label for="code">Code:</label>
			<div class="input">
				<@spring.bind "formation.code" />
				<input id="code" type="text" name="code" required value="${(formation.code)! ""}"/>
			</div>
			<div>
			    <@spring.showErrors separator=",<br> " classOrStyle="error"/>
			    <#if duplicateCode??>
			    	<span class="error">Le code que vous avez choisie existe déjà</span>
				</#if>
	        </div>
		</div>
		<div class="fieldWrapper">
			<label for="name">Nom:</label>
			<div class="input">
				<input id="name" type="text" name="name" required value="${(formation.name)! ""}"/>
			</div>
			<div>
			    <@spring.showErrors separator=",<br> " classOrStyle="error"/>
	        </div>
		</div>
		<div class="fieldWrapper">
		<label for="nature">Nature:</label>
			<div class="input">
				<SELECT id="nature" name="nature" required>
					<option value="">Aucune</option>
					<#list natures>
						<#items as nature>
							<#if (formation.nature.idNature)??>
								<#if formation.nature.idNature == nature.idNature>
									<option value = "${nature.idNature}" selected>${nature.name}</option>
								<#else>
									<option value = "${nature.idNature}">${nature.name}</option>
								</#if>
							<#else>
								<option value = "${nature.idNature}">${nature.name}</option>
							</#if>
						</#items>
					</#list>
				</SELECT>
			</div>
		</div>
		<div class="fieldWrapper">
		<label for="composante">Composante:</label><div class="input"><SELECT id="composante" name="composante" required>
									<option value="">Aucune</option>
								<#list composantes>
								<#items as composante>
								<#if (formation.composante.idComposante)??>
									<#if formation.composante.idComposante == composante.idComposante>
									<option value = "${composante.idComposante}" selected>${composante.name}</option>
									<#else>
									<option value = "${composante.idComposante}">${composante.name}</option>
									</#if>
								<#else>
									<option value = "${composante.idComposante}">${composante.name}</option>
								</#if>
								</#items>
								</#list>
								</SELECT></div>
		</div>
	</fieldset>
	<fieldset>
		<legend><h3>Paramètres</h3></legend>
		<div class="fieldWrapper">
			<label for="CMGroupSize">Taille des groupes de CM:</label><div class="input"><input id="CMGroupSize" type="number" name="CMGroupSize" min="0" value="${(formation.CMGroupSize)! ""}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="TDGroupSize">Taille des groupes de TD:</label><div class="input"><input id="TDGroupSize" type="number" name="TDGroupSize" min="0" required value="${(formation.TDGroupSize)}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="TPGroupSize">Taille des groupes de TP:</label><div class="input"><input id="TPGroupSize" type="number" name="TPGroupSize" min="0" required value="${(formation.TPGroupSize)}"/></div>
		</div>
		<!--<label for="sites">Sites d'enseignement:</label></br>
			<input type="checkbox" name="site" value="site1"/>Site1</br>
			<input type="checkbox" name="site" value="site2"/>Site2</br>
			<input type="checkbox" name="site" value="site3"/>Site3</br>-->
		<#setting locale="en_US">
		<div class="fieldWrapper">
			<label for="numberOfHoursPerCredit">Nombre d'heures par crédit:</label><div class="input"><input id="numberOfHoursPerCredit" type="number" name="numberOfHoursPerCredit" min="0" step="0.1" required value="${(formation.numberOfHoursPerCredit)}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="percentOfCM">Pourcentage de CM:</label><div class="input"><input id="percentOfCM" type="number" name="percentOfCM" min="0" max="100" step="0.1" required value="${formation.percentOfCM}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="percentOfTD">Pourcentage de TD:</label><div class="input"><input id="percentOfTD" type="number" name="percentOfTD" min="0" max="100" step="0.1" required value="${formation.percentOfTD}"/></div>
		</div>
		<div class="fieldWrapper">
			<label for="percentOfTP">Pourcentage de TP:</label><div class="input"><input id="percentOfTP" type="number" name="percentOfTP" min="0" max="100" step="0.1" required value="${(formation.percentOfTP)}"/></div>
		</div>
	</fieldset>
	<input id="submit" type="submit" value="Sauvegarder"/>
	<input Type="button" value="Retour" onClick="history.go(-1);return true;">
	<!--<a id="cancel" href="/formations">Annuler</a>-->
</form>
</@layout.baseLayout>