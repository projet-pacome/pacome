package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.univ.amu.pacome.entities.tree.Site;

@Repository
public interface SiteRepository extends JpaRepository<Site, Integer> {
	public List<Site> findByNameContainingIgnoreCase(String name);
}
