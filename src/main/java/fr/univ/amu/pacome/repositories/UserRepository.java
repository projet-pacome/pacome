package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univ.amu.pacome.entities.user.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public List<User> findByLastNameContainingIgnoreCase (String lastName);
	public List<User> findByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase (String lastName, String firstName);
}
