package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.univ.amu.pacome.entities.user.Formation;

@Repository
public interface FormationRepository extends JpaRepository<Formation, Integer> {

	public List<Formation> findByNameContainingIgnoreCase(String name);
	
}
