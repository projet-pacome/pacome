package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.univ.amu.pacome.entities.user.Nature;

@Repository
public interface NatureRepository  extends JpaRepository<Nature, Integer> {

	public List<Nature> findByNameContainingIgnoreCase(String name); // TODO : Pertinent de mettre une liste ici ?
}
