package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Type;

@Repository
public interface NodeRepository  extends JpaRepository<Node, Integer> {

	public List<Node> findByNameContainingIgnoreCase (String name);
	public List<Node> findByCode (String code);
	public List<Node> findByType (Type type);
}

