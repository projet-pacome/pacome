package fr.univ.amu.pacome.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.univ.amu.pacome.entities.user.Composante;

@Repository
public interface ComposanteRepository extends JpaRepository<Composante, Integer> {

	public List<Composante> findByNameContainingIgnoreCase(String name);
	
}