package fr.univ.amu.pacome.entities.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "natures")
public class Nature implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idNature;

	@NotNull
	private String name;

	public Nature() {
		
	}

	public Integer getIdNature() {
		return idNature;
	}

	public void setIdNature(Integer idNature) {
		this.idNature = idNature;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}