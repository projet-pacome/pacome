package fr.univ.amu.pacome.entities.user;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Site;

@Entity
@Table(name = "formations")
public class Formation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idFormation;

	@NotNull
	@Column(unique = true)
	@Size(min = 3, max = 20, message = "Le code doit étre compris entre 3 et 20 caractéres")
	@Pattern(regexp = "^[A-Z][A-Z0-9]{3,20}+$", message = "Le code doit contenir seulement des majucules et des chiffres")
	private String code;

	@NotNull
	@Size(min = 3, max = 200, message = "Le nom doit étre compris entre 3 et 200 caractéres")
//	@Pattern(regexp = "^[a-zA-Z0-9éèàêâùïüëç#[/s]]#{3,200}+$", message = "Le nom doit contenir seulement des lettres majucules/miniscules ou/et des chiffres")
	private String name;
	
	@OneToOne
	private Nature nature;

	@OneToOne
	@JoinColumn(name = "id_node")
	private Node root;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "formation_user", joinColumns = @JoinColumn(name = "id_formation", referencedColumnName = "idFormation"), inverseJoinColumns = @JoinColumn(name = "id_user", referencedColumnName = "idUser"))
	private Set<User> users;

	@ManyToOne
	private Composante composante;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "formation_site", joinColumns = @JoinColumn(name = "id_formation", referencedColumnName = "idFormation"), inverseJoinColumns = @JoinColumn(name = "id_site", referencedColumnName = "idSite"))
	private Set<Site> sites;

	@Enumerated(EnumType.STRING)
	@NotNull
	private ValidationState validationState = ValidationState.UNKNOWN;

	@NotNull
	private Boolean isEnabled = true;

	private Integer CMGroupSize;
	private Integer TDGroupSize;
	private Integer TPGroupSize;
	private Float numberOfHoursPerCredit;

	@DecimalMax("100.0")
	private Float percentOfCM;
	@DecimalMax("100.0")
	private Float percentOfTD;
	@DecimalMax("100.0")
	private Float percentOfTP;

	@OneToMany
	private Set<Node> nodes;

	public Formation() {

	}

	public Set<Site> getSites() {
		return sites;
	}

	public void setSites(Set<Site> sites) {
		this.sites = sites;
	}

	public Nature getNature() {
		return nature;
	}

	public void setNature(Nature nature) {
		this.nature = nature;
	}

	public Composante getComposante() {
		return composante;
	}

	public void setComposante(Composante composante) {
		this.composante = composante;
	}

	public Integer getIdFormation() {
		return idFormation;
	}

	public void setIdFormation(Integer idFormation) {
		this.idFormation = idFormation;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Boolean getActivationState() {
		return isEnabled;
	}

	public void setActivationState(Boolean activationState) {
		this.isEnabled = activationState;
	}

	public Integer getCMGroupSize() {
		return CMGroupSize;
	}

	public void setCMGroupSize(Integer cMGroupSize) {
		CMGroupSize = cMGroupSize;
	}

	public Integer getTDGroupSize() {
		return TDGroupSize;
	}

	public ValidationState getValidationState() {
		return validationState;
	}

	public void setValidationState(ValidationState validationState) {
		this.validationState = validationState;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public void setTDGroupSize(Integer tDGroupSize) {
		TDGroupSize = tDGroupSize;
	}

	public Integer getTPGroupSize() {
		return TPGroupSize;
	}

	public void setTPGroupSize(Integer tPGroupSize) {
		TPGroupSize = tPGroupSize;
	}

	public Set<Node> getNodes() {
		return nodes;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}

	public Float getNumberOfHoursPerCredit() {
		return numberOfHoursPerCredit;
	}

	public void setNumberOfHoursPerCredit(Float numberOfHoursPerCredit) {
		this.numberOfHoursPerCredit = numberOfHoursPerCredit;
	}

	public Float getPercentOfCM() {
		return percentOfCM;
	}

	public void setPercentOfCM(Float percentOfCM) {
		this.percentOfCM = percentOfCM;
	}

	public Float getPercentOfTD() {
		return percentOfTD;
	}

	public void setPercentOfTD(Float percentOfTD) {
		this.percentOfTD = percentOfTD;
	}

	public Float getPercentOfTP() {
		return percentOfTP;
	}

	public void setPercentOfTP(Float percentOfTP) {
		this.percentOfTP = percentOfTP;
	}

	@Override
	public String toString() {
		return "Formation [idFormation=" + idFormation + ", code=" + code + ", name=" + name + ", nature=" + nature
				+ ", root=" + root + ", users=" + users + ", composante=" + composante + ", sites=" + sites
				+ ", validationState=" + validationState + ", isEnabled=" + isEnabled + ", CMGroupSize=" + CMGroupSize
				+ ", TDGroupSize=" + TDGroupSize + ", TPGroupSize=" + TPGroupSize + ", numberOfHoursPerCredit="
				+ numberOfHoursPerCredit + ", percentOfCM=" + percentOfCM + ", percentOfTD=" + percentOfTD
				+ ", percentOfTP=" + percentOfTP + "]\n";
	}
}
