package fr.univ.amu.pacome.entities.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import fr.univ.amu.pacome.entities.user.Formation;

@Entity
@Table(name = "nodes")
public class Node implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idNode;

	@NotNull
	private String name;

	@NotNull
//	@Column(unique = true)
//	@Size(min = 3, max = 20, message = "Le code doit étre compris entre 3 et 20 caractéres")
//	@Pattern(regexp = "^[A-Z][A-Z0-9]{3,20}+$", message = "Le code doit contenir seulement des majucules et des chiffres")
	private String code;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Type type;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<Node> children = new ArrayList<Node>();

	@NotNull
	private Float credits = 0.0f;

	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	@MapKeyColumn(name = "site_id")
	private Map<Site, Integer> sites = new HashMap<Site, Integer>();

	private Float HCM = 0.0f;
	private Float HTD = 0.0f;
	private Float HTP = 0.0f;
	private Float HNP = 0.0f;
	private Float HETD = 0.0f;
	private Float HETDComputed = 0.0f;

	private Integer nbGroupsCM = 0;
	private Integer nbGroupsTD = 0;
	private Integer nbGroupsTP = 0;

	@ElementCollection
	private List<String> errorMsg = new ArrayList<String>();

	@ManyToOne
	private Formation formation;

	public Node() {

	}

	public Integer getIdNode() {
		return idNode;
	}

	public void setIdNode(Integer idNode) {
		this.idNode = idNode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public Float getCredits() {
		return credits;
	}

	public void setCredits(Float credits) {
		this.credits = credits;
	}

	public Float getHCM() {
		return HCM;
	}

	public void setHCM(Float hCM) {
		HCM = hCM;
	}

	public Float getHTD() {
		return HTD;
	}

	public void setHTD(Float hTD) {
		HTD = hTD;
	}

	public Float getHTP() {
		return HTP;
	}

	public void setHTP(Float hTP) {
		HTP = hTP;
	}

	public Float getHNP() {
		return HNP;
	}

	public void setHNP(Float hNP) {
		HNP = hNP;
	}

	public Float getHETD() {
		return HETD;
	}

	public void setHETD(Float hETD) {
		HETD = hETD;
	}

	public Float getHETDComputed() {
		return HETDComputed;
	}

	public void setHETDComputed(Float hETDComputed) {
		HETDComputed = hETDComputed;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Integer getNbGroupsCM() {
		return nbGroupsCM;
	}

	public void setNbGroupsCM(Integer nbGroupsCM) {
		this.nbGroupsCM = nbGroupsCM;
	}

	public Integer getNbGroupsTD() {
		return nbGroupsTD;
	}

	public void setNbGroupsTD(Integer nbGroupsTD) {
		this.nbGroupsTD = nbGroupsTD;
	}

	public Integer getNbGroupsTP() {
		return nbGroupsTP;
	}

	public void setNbGroupsTP(Integer nbGroupsTP) {
		this.nbGroupsTP = nbGroupsTP;
	}

	public List<String> getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(List<String> errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Map<Site, Integer> getSites() {
		return sites;
	}

	public void setSites(Map<Site, Integer> sites) {
		this.sites = sites;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((HCM == null) ? 0 : HCM.hashCode());
		result = prime * result + ((HETD == null) ? 0 : HETD.hashCode());
		result = prime * result + ((HNP == null) ? 0 : HNP.hashCode());
		result = prime * result + ((HTD == null) ? 0 : HTD.hashCode());
		result = prime * result + ((HTP == null) ? 0 : HTP.hashCode());
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((errorMsg == null) ? 0 : errorMsg.hashCode());
		result = prime * result + ((formation == null) ? 0 : formation.hashCode());
		result = prime * result + ((idNode == null) ? 0 : idNode.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nbGroupsCM == null) ? 0 : nbGroupsCM.hashCode());
		result = prime * result + ((nbGroupsTD == null) ? 0 : nbGroupsTD.hashCode());
		result = prime * result + ((nbGroupsTP == null) ? 0 : nbGroupsTP.hashCode());
		result = prime * result + ((credits == null) ? 0 : credits.hashCode());
		result = prime * result + ((sites == null) ? 0 : sites.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (HCM == null) {
			if (other.HCM != null)
				return false;
		} else if (!HCM.equals(other.HCM))
			return false;
		if (HETD == null) {
			if (other.HETD != null)
				return false;
		} else if (!HETD.equals(other.HETD))
			return false;
		if (HNP == null) {
			if (other.HNP != null)
				return false;
		} else if (!HNP.equals(other.HNP))
			return false;
		if (HTD == null) {
			if (other.HTD != null)
				return false;
		} else if (!HTD.equals(other.HTD))
			return false;
		if (HTP == null) {
			if (other.HTP != null)
				return false;
		} else if (!HTP.equals(other.HTP))
			return false;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (errorMsg == null) {
			if (other.errorMsg != null)
				return false;
		} else if (!errorMsg.equals(other.errorMsg))
			return false;
		if (formation == null) {
			if (other.formation != null)
				return false;
		} else if (!formation.equals(other.formation))
			return false;
		if (idNode == null) {
			if (other.idNode != null)
				return false;
		} else if (!idNode.equals(other.idNode))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nbGroupsCM == null) {
			if (other.nbGroupsCM != null)
				return false;
		} else if (!nbGroupsCM.equals(other.nbGroupsCM))
			return false;
		if (nbGroupsTD == null) {
			if (other.nbGroupsTD != null)
				return false;
		} else if (!nbGroupsTD.equals(other.nbGroupsTD))
			return false;
		if (nbGroupsTP == null) {
			if (other.nbGroupsTP != null)
				return false;
		} else if (!nbGroupsTP.equals(other.nbGroupsTP))
			return false;
		if (credits == null) {
			if (other.credits != null)
				return false;
		} else if (!credits.equals(other.credits))
			return false;
		if (sites == null) {
			if (other.sites != null)
				return false;
		} else if (!sites.equals(other.sites))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Node [idNode=" + idNode + ", name=" + name + ", code=" + code + ", type=" + type + ", children="
				+ children + ", credits=" + credits + ", sites=" + sites + ", HCM=" + HCM + ", HTD=" + HTD + ", HTP="
				+ HTP + ", HNP=" + HNP + ", HETD=" + HETD + ", nbGroupsCM=" + nbGroupsCM + ", nbGroupsTD=" + nbGroupsTD
				+ ", nbGroupsTP=" + nbGroupsTP + ", errorMsg=" + errorMsg + ", formation=" + formation + "]";
	}

}