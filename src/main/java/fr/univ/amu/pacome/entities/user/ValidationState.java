package fr.univ.amu.pacome.entities.user;

public enum ValidationState {
	VALID, INVALID, UNKNOWN;
}
