package fr.univ.amu.pacome.entities.tree;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import fr.univ.amu.pacome.entities.user.Formation;

@Entity
@Table(name = "sites")
public class Site implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idSite;

	@NotNull
	private String name;

	@ManyToMany(mappedBy = "sites", cascade = CascadeType.ALL)
	private Set<Formation> formations;

	public Site() {
	}

	public Integer getIdSite() {
		return idSite;
	}

	public void setIdSite(Integer idSite) {
		this.idSite = idSite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Formation> getFormations() {
		return formations;
	}

	public void setFormations(Set<Formation> formations) {
		this.formations = formations;
	}

}