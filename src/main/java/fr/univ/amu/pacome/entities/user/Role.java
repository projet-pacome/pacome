package fr.univ.amu.pacome.entities.user;

public enum Role {
	ADMIN, OBSERVER;
}