package fr.univ.amu.pacome.entities.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
	
	@SuppressWarnings("serial")
	private static final ArrayList<Type> FRMson = new ArrayList<Type>(){{
		add(Type.PT);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> PTson = new ArrayList<Type>(){{
		add(Type.AN);
		add(Type.SE);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> ANson = new ArrayList<Type>(){{
		add(Type.SE);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> SEson = new ArrayList<Type>(){{
		add(Type.LI);
		add(Type.OP);
		add(Type.UE);
		add(Type.UEC);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> LIson = new ArrayList<Type>(){{
		add(Type.OP);
		add(Type.LI);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> OPson = new ArrayList<Type>(){{
		add(Type.UE);
		add(Type.LI);
		add(Type.UEC);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> UEson = new ArrayList<Type>(){{
		add(Type.UEC);
	}};

	@SuppressWarnings("serial")
	private static final ArrayList<Type> UECson = new ArrayList<Type>(){{
		add(Type.UEC);
	}};
	
	@SuppressWarnings("serial")
	private static Map<Type, List<Type>> linkBetweenTwoTypeBis = new HashMap<Type, List<Type>>(){{
		put(Type.FRM, FRMson);
		put(Type.PT , PTson);
		put(Type.AN , ANson);
		put(Type.SE , SEson);
		put(Type.LI , LIson);
		put(Type.OP , OPson);
		put(Type.UE , UEson);
	    put(Type.UEC, UECson);
	}};

	
	public static Map<Type, List<Type>> getLinkBetweenTwoType() {
		return linkBetweenTwoTypeBis;
	}
}
