package fr.univ.amu.pacome.entities.tree;

public enum Type {
	FRM, PT, UE, UEC, AN, SE, LI, OP, EC;
}