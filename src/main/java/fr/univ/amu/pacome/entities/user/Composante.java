package fr.univ.amu.pacome.entities.user;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "composantes")
public class Composante implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idComposante;

	@NotNull
	private String code;

	@NotNull
	private String name;

	@OneToMany(mappedBy = "composante", cascade = CascadeType.ALL)
	private Set<User> users;

	@OneToMany(mappedBy = "composante", cascade = CascadeType.ALL)
	private Set<Formation> formations;

	@NotNull
	private float conversionRateTPToHETD;

	public Composante() {

	}

	public Integer getIdComposante() {
		return idComposante;
	}

	public void setIdComposante(Integer idComposante) {
		this.idComposante = idComposante;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Formation> getFormations() {
		return formations;
	}

	public void setFormations(Set<Formation> formations) {
		this.formations = formations;
	}

	public float getConversionRateTPToHETD() {
		return conversionRateTPToHETD;
	}

	public void setConversionRateTPToHETD(float conversionRateTPToHETD) {
		this.conversionRateTPToHETD = conversionRateTPToHETD;
	}

}