package fr.univ.amu.pacome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacomeApplication.class, args);
	}
}