package fr.univ.amu.pacome.services.computing;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Site;
import fr.univ.amu.pacome.entities.tree.Type;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.managers.NodeManager;
import fr.univ.amu.pacome.managers.SitesManager;
@PropertySource(value = "classpath:application-configuration.properties")
@Service
public class ComputeNbGroups {
	@Autowired
	NodeManager nm;
	@Autowired
	SitesManager sm;
	private ComputingCore core;
	@Value("${default.formation.group_td_size}")
	private int groupTDSize;
	@Value("${default.formation.group_tp_size}")
	private int groupTPSize;
	private int groupCMSize = 0;
	@Value("${default.formation.number_hours_per_credits}")
	private int nbHoursPerCredits;
	private float percentOfCM;
	private float percentOfTD;
	private float percentOfTP;
	public ComputeNbGroups() {
	}
	public ComputeNbGroups(ComputingCore core) {
		this.core = core;
	}
	public ComputingCore getCore() {
		return core;
	}
	public void setCore(ComputingCore core) {
		this.core = core;
	}
	public void compute(Formation formation) {
		Node root = formation.getRoot();
		if (formation.getCMGroupSize() != null)
			this.groupCMSize = formation.getCMGroupSize();
		if (formation.getTDGroupSize() != null)
			this.groupTDSize = formation.getTDGroupSize();
		if (formation.getTPGroupSize() != null)
			this.groupTPSize = formation.getTPGroupSize();
		this.percentOfCM = formation.getPercentOfCM();
		this.percentOfTD = formation.getPercentOfTD();
		this.percentOfTP = formation.getPercentOfTP();
		browseTree(root);
	}
	public void ComputeGroupsNb(Node node) {
		int effectifs;
		Site site = sm.findOne(1);
		if (!node.getType().equals(Type.FRM)) {
			if (this.groupCMSize == 0) {
				node.setNbGroupsCM(1);
			} else if (this.groupCMSize != 0) {
				effectifs = node.getSites().get(site);
				double nbGroups = (double) effectifs / this.groupCMSize;
				node.setNbGroupsCM((int) Math.ceil(nbGroups));
			}
			if (this.groupTDSize == 0) {
				node.setNbGroupsTD(1);
			} else if (this.groupTDSize != 0) {
				effectifs = node.getSites().get(site);
				double nbGroups = (double) effectifs / this.groupTDSize;
				node.setNbGroupsTD((int) Math.ceil(nbGroups));
			}
			if (this.groupTPSize == 0) {
				node.setNbGroupsTP(1);
			} else if (this.groupTPSize != 0) {
				effectifs = node.getSites().get(site);
				double nbGroups = (double) effectifs / this.groupTPSize;
				node.setNbGroupsTP((int) Math.ceil(nbGroups));
			}
		} else {
			node.setNbGroupsTP(0);
			node.setNbGroupsTP(0);
			node.setNbGroupsTP(0);
		}
	}
	public void calculHoursCMTDTP(Node node) {
		if (!node.getType().equals(Type.FRM)) {
			node.setHCM(
					(float) node.getNbGroupsCM() * ((nbHoursPerCredits * node.getCredits() * this.percentOfCM) / 100));
			node.setHTD(
					(float) node.getNbGroupsTD() * ((nbHoursPerCredits * node.getCredits() * this.percentOfTD) / 100));
			node.setHTP(
					(float) node.getNbGroupsTP() * ((nbHoursPerCredits * node.getCredits() * this.percentOfTP) / 100));
		}
//		System.out.println(" node id :  " + node.getIdNode() + "  HCM  : " + node.getHCM());
//		System.out.println(" node id :  " + node.getIdNode() + "  HCM  : " + node.getHTD());
//		System.out.println(" node id :  " + node.getIdNode() + "  HCM  : " + node.getHTP());

	}
	public void browseTree(Node root) {
		Site site = sm.findOne(1);
		Integer effectif;
		HashMap<Site, Integer> effectifs = new HashMap<>();
		for (Node child : root.getChildren()) {
			effectif = root.getSites().get(site);
			if (!root.getChildren().isEmpty() && effectif != null) {
				effectifs.put(site, effectif);
				child.setSites(effectifs);
			}
//			System.out.println("La valeur de l'effectif du site Luminy du Noeud  :  " + child.getName() + " est : "
//					+ child.getSites().get(site).intValue());
			this.ComputeGroupsNb(child);
			this.calculHoursCMTDTP(child);
//			System.out.println("child : " + child.getType() + " nb groupes CM : " + child.getNbGroupsCM()
//					+ " nb groupes TD : " + child.getNbGroupsTD() + " nb groupes TP : " + child.getNbGroupsTP());
			browseTree(child);
		}
	}
}