package fr.univ.amu.pacome.services.computing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Site;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.managers.NodeManager;
import fr.univ.amu.pacome.managers.SitesManager;

@PropertySource(value = "classpath:application-configuration.properties")
@Service
public class ComputeHETD {

	@Autowired
	NodeManager nm;

	@Autowired
	SitesManager sm;

	@Value("${configuration.calcul.cm_to_hetd}")
	private String tauxCmHetd;
	
	@Value("${configuration.calcul.td_to_hetd}")
	private String tauxTdHetd;
	
	@Value("${configuration.calcul.tp_to_hetd}")
	private String tauxTpHetd;
	
	@Value("${default.formation.number_hours_per_credits}")
	private int nbHoursPerCredits;

	public Float getTauxTdHetd() {
		return Float.parseFloat(tauxTdHetd);
	}

	public void setTauxTdHetd(String tauxTdHetd) {
		this.tauxTdHetd = tauxTdHetd;
	}

	public Float getTauxTpHetd() {
		return Float.parseFloat(tauxTpHetd);
	}

	public void setTauxTpHetd(String tauxTpHetd) {
		this.tauxTpHetd = tauxTpHetd;
	}

	public float getTauxCmHetd() {
		return Float.parseFloat(tauxCmHetd);
	}

	public void setTauxCmHetd(String tauxCmHetd) {
		this.tauxCmHetd = tauxCmHetd;
	}

	public ComputeHETD() {

	}

	public Float computeHETD(Formation formation, Node node) {
		Site site = sm.findOne(1);
		Float HETD = 0.0f;
		
		if (node.getHETD() == 0.0f ) {
			
			HETD = ((node.getHCM() * this.getTauxCmHetd()) + (node.getHTP() * this.getTauxTpHetd())
					+ (node.getHTD() * this.getTauxTdHetd()) + (node.getHNP() * node.getSites().get(site)));
			node.setHETDComputed(HETD);
		} else {
			return node.getHETDComputed();
		}
		return HETD;
	}

	public Float sumHETD(Node node) {
		Float sumHETD = 0f;
		for (int i = 0; i < node.getChildren().size(); i++) {
			sumHETD += node.getChildren().get(i).getHETDComputed();
		}
		return sumHETD;
	}

	public void browseTree(Formation formation, Node root) {
		Float HETD = 0f;
		for (Node child : root.getChildren()) {
			if (child.getChildren().isEmpty()) {
				HETD = this.computeHETD(formation, child);
				child.setHETDComputed(HETD);
			}
			browseTree(formation, child);
		}
		if (!root.getChildren().isEmpty()) {
			root.setHETDComputed(this.sumHETD(root));
		}
	}
}