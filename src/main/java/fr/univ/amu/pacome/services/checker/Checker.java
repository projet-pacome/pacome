package fr.univ.amu.pacome.services.checker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;

@Service
public class Checker {

	private Class<?> classe;
	private String a = null;

	public Boolean validate(Formation frm)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {

		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new AnnotationTypeFilter(Check.class));
		for (BeanDefinition beanDef : provider.findCandidateComponents("fr.univ.amu.pacome.services.checker")) {
			a = beanDef.getBeanClassName();
			classe = Class.forName(a);
			Object obj = classe.newInstance();
			Method method = classe.getMethod("validate", Formation.class);
			if (!(Boolean) method.invoke(obj, frm)) {
				frm.setValidationState(ValidationState.INVALID);
				return false;
			}
		}

		frm.setValidationState(ValidationState.VALID);
		return true;
	}

}
