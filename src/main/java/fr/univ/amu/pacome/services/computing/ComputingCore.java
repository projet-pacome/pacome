package fr.univ.amu.pacome.services.computing;

import java.lang.reflect.InvocationTargetException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;
import fr.univ.amu.pacome.services.checker.Checker;

@Service
public class ComputingCore {

	@Autowired
	private ComputeNbGroups computeNbGroups;

	@Autowired
	private ComputeHETD computeHETD;

	@Autowired
	private Checker checker;

	public ComputingCore() {
	}

	public void estimateFormationHETD(Formation formation)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		if (formation.getValidationState().equals(ValidationState.UNKNOWN)) {
			checker.validate(formation);
		}
		if (formation.getValidationState().equals(ValidationState.VALID)) {
			computeNbGroups.compute(formation);
			computeHETD.browseTree(formation, formation.getRoot());
		}
	}

	public ComputeNbGroups getComputeNbGroups() {
		return computeNbGroups;
	}

	public void setComputeNbGroups(ComputeNbGroups computeNbGroups) {
		this.computeNbGroups = computeNbGroups;
	}

	public ComputeHETD getComputeHETD() {
		return computeHETD;
	}

	public void setComputeHETD(ComputeHETD computeHETD) {
		this.computeHETD = computeHETD;
	}
}
