package fr.univ.amu.pacome.services.checker;

import fr.univ.amu.pacome.entities.user.Formation;

import java.util.ArrayList;
import java.util.List;

import fr.univ.amu.pacome.entities.tree.*;

@Check
public class CheckStructure implements IChecker {

	Boolean checkResult ;

	@Override
	public Boolean validate(Formation frm) {
		checkResult = true;
		browseTree(frm.getRoot());
		return checkResult;

	}

	public Boolean browseTree(Node root) {
		for (Node child : root.getChildren()) {
			List<String> errorMsg = new ArrayList<String>();
			
			if (root.getType() == Type.FRM && !child.getType().toString().matches("(\\bPT\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que PT comme fils");
				root.setErrorMsg(errorMsg);
			}
			
			if (root.getType() == Type.PT && !child.getType().toString().matches("(\\bAN\\b)|(\\bSE\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que AN, SE comme fils");
				root.setErrorMsg(errorMsg);
			}

			if (root.getType() == Type.AN && !child.getType().toString().matches("(\\SE\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que PT comme fils");
				root.setErrorMsg(errorMsg);
				
			}
			if (root.getType() == Type.AN && root.getChildren().size()!=2){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "doit avoir exactement deux semestres");
				root.setErrorMsg(errorMsg);
			}

			if (root.getType() == Type.SE && !child.getType().toString().matches("(\\bLI\\b)|(\\bOP\\b)|(\\bUE\\b)|(\\bUEC\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que LI, OP, UE, UEC comme fils");
				root.setErrorMsg(errorMsg);
			}
			
			if (root.getType() == Type.LI && !child.getType().toString().matches("(\\bOP\\b)|(\\bUE\\b)|(\\bUEC\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que OP, UE, UEC comme fils");
				root.setErrorMsg(errorMsg);
			}
			
			if (root.getType() == Type.OP && !child.getType().toString().matches("(\\bLI\\b)|(\\bUE\\b)|(\\bUEC\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que LI, UE, UEC comme fils");
				root.setErrorMsg(errorMsg);
			}

			if (root.getType() == Type.UE && !child.getType().toString().matches("(\\bUEC\\b)")){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut avoir que UEC comme fils");
				root.setErrorMsg(errorMsg);
			}

			if (root.getType() == Type.EC){
				checkResult = false;
				errorMsg.add("Le noeud  : "+root.getName()+"  de type :"+root.getType()+ "ne peut pas avoir de fils ");
				root.setErrorMsg(errorMsg);
			}

			browseTree(child);

		}

		return checkResult;
	}
}
