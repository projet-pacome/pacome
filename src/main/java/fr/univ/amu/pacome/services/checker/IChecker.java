package fr.univ.amu.pacome.services.checker;

import fr.univ.amu.pacome.entities.user.Formation;

public interface IChecker {
	public Boolean validate(Formation frm);
}
