package fr.univ.amu.pacome.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePageController {

	@RequestMapping("/")
	String home(Model model) {

		model.addAttribute("user", "Unauthenticated user (auth not implemented)");
		return "homepage";
	}

}
