package fr.univ.amu.pacome.web;

import java.lang.reflect.InvocationTargetException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Type;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;
import fr.univ.amu.pacome.managers.ComposantesManager;
import fr.univ.amu.pacome.managers.FormationsManager;
import fr.univ.amu.pacome.managers.NaturesManager;
import fr.univ.amu.pacome.managers.NodeManager;
import fr.univ.amu.pacome.services.computing.ComputeHETD;
import fr.univ.amu.pacome.services.computing.ComputingCore;

@Controller()
@RequestMapping("/formations")
@SessionAttributes({ "formation", "element" })
@PropertySource(value = "classpath:application-configuration.properties")
public class FormationsController {
	@Autowired
	FormationsManager fm;
	@Autowired
	NodeManager nodem;
	@Autowired
	NaturesManager nm;
	@Autowired
	ComposantesManager cm;
	@Autowired
	ComputeHETD computeHETD;
	@Autowired
	ComputingCore computingCore;
	@Value("${default.formation.group_tp_size}")
	private Integer TDGroupSize;
	@Value("${default.formation.group_tp_size}")
	private Integer TPGroupSize;
	@Value("${default.formation.number_hours_per_credits}")
	private String numberOfHoursPerCredit;
	@Value("${default.formation.cm_percentage}")
	private String percentOfCM;
	@Value("${default.formation.td_percentage}")
	private String percentOfTD;
	@Value("${default.formation.tp_percentage}")
	private String percentOfTP;

	@RequestMapping("")
	String showFormationList(Model model) {
		model.addAttribute("formations", fm.findAll());
		return "formation-list";
	}

	@GetMapping("/{formationId}")
	String showFormation(@PathVariable("formationId") int formationId, Model model, HttpServletResponse response) {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		model.addAttribute("formation", formation);
		return "formation-details";
	}

	@RequestMapping("/create")
	String createFormation(Model model) {
		Formation formation = new Formation();
		formation.setTDGroupSize(TDGroupSize);
		formation.setTPGroupSize(TPGroupSize);
		formation.setActivationState(false);
		formation.setNumberOfHoursPerCredit(Float.parseFloat(numberOfHoursPerCredit));
		formation.setPercentOfCM(Float.parseFloat(percentOfCM));
		formation.setPercentOfTD(Float.parseFloat(percentOfTD));
		formation.setPercentOfTP(Float.parseFloat(percentOfTP));
		formation.setValidationState(ValidationState.UNKNOWN);
		formation.setActivationState(true);
		// formation.setSeuil(seuil);
		model.addAttribute("formation", formation);
		model.addAttribute("natures", nm.findAll());
		model.addAttribute("composantes", cm.findAll());
		return "formation-form";
	}

	@PostMapping("/save")
	String saveFormation(@ModelAttribute("formation") @Valid Formation formation, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("natures", nm.findAll());
			model.addAttribute("composantes", cm.findAll());
			return "formation-form";
		}
		try {
			fm.createFRMNode(formation);
			fm.save(formation);
		} catch (Exception e) {
			model.addAttribute("natures", nm.findAll());
			model.addAttribute("composantes", cm.findAll());
			model.addAttribute("duplicateCode", true);
			return "formation-form";
		}
		return "redirect:/formations";
	}

	@GetMapping("{formationId}/edit")
	String editFormation(@PathVariable("formationId") int formationId, Model model, HttpServletResponse response) {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		model.addAttribute("formation", formation);
		model.addAttribute("natures", nm.findAll());
		model.addAttribute("composantes", cm.findAll());
		return "formation-form";
	}

	@GetMapping("{formationId}/nodes/{fatherId}/new-child")
	String createNode(@PathVariable("formationId") int formationId, @PathVariable("fatherId") int fatherId,
			Model model) {
		model.addAttribute("element", new Node());
		model.addAttribute("types", Type.values());
		model.addAttribute("formationId", formationId);
		model.addAttribute("fatherId", fatherId);
		return "element-form";
	}

	@PostMapping("{formationId}/nodes/{fatherId}/new-child")
	String saveNewNode(@PathVariable("formationId") int formationId, HttpServletResponse response,
			@PathVariable("fatherId") int fatherId, @ModelAttribute("element") Node element) {
		/*
		 * commented, for futur purpose
		 * element.setFormation(fm.findOne(formationId));
		 */
		Node father = nodem.findOne(fatherId);
		if (father == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		fm.save(fm.findOne(formationId));
		father.getChildren().add(nodem.save(element));
		nodem.save(father);
		return "redirect:/formations/" + formationId + "/tree";
	}

	@GetMapping("{formationId}/nodes/{fatherId}/delete/{nodeId}")
	String deleteNode(@PathVariable("formationId") int formationId, @PathVariable("fatherId") int fatherId,
			@PathVariable("nodeId") int nodeId) {
		try {
			nodem.deleteCascade(fatherId, nodeId);
		} catch (Exception e) {
			// TODO remove this dirty hack used to ignore MySQLIntegrityConstraintViolationException
			// that's why it doesn't actually delete the nodes, just detach them...
		}
		return "redirect:/formations/" + formationId + "/tree";
	}

	@GetMapping("{formationId}/nodes/{nodeId}/edit")
	String editNode(@PathVariable("nodeId") int nodeId, Model model) {
		model.addAttribute("element", nodem.findOne(nodeId));
		model.addAttribute("types", Type.values());
		return "element-form";
	}

	@PostMapping("{formationId}/nodes/{nodeId}/edit")
	String saveNode(@PathVariable("formationId") int formationId, Model model, @ModelAttribute("element") Node node) {
		nodem.save(node);
		return "redirect:/formations/" + formationId + "/tree";
	}

	@GetMapping("{formationId}/toggle-state")
	String switchFormationActivationState(@PathVariable("formationId") int formationId, HttpServletResponse response) {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		formation.setActivationState(!formation.getActivationState());
		fm.saveWithoutAlteringValidationState(formation);
		return "redirect:/formations";
	}

	@GetMapping("{formationId}/computeHETD")
	String computeHETD(@PathVariable("formationId") int formationId, HttpServletResponse response, Model model)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		computingCore.estimateFormationHETD(formation);
		fm.saveWithoutAlteringValidationState(formation);
		model.addAttribute("formation", formation);
		return "redirect:/formations/" + formationId;
	}

	@GetMapping("{formationId}/validate")
	String validateFormation(@PathVariable("formationId") int formationId, HttpServletResponse response, Model model)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		fm.validate(formation);
		fm.saveWithoutAlteringValidationState(formation);
		model.addAttribute("formation", formation);
		return "redirect:/formations/" + formationId;
	}

	@GetMapping("{formationId}/tree")
	String tree(@PathVariable("formationId") int formationId, Model model) {
		model.addAttribute("formation", fm.findOne(formationId));
		model.addAttribute("types", Type.values());
		return "formation-tree";
	}

	@GetMapping("{formationId}/tree.json")
	@ResponseBody
	Node treeJson(@PathVariable("formationId") int formationId, HttpServletResponse response) {
		Formation formation = fm.findOne(formationId);
		if (formation == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.setContentType(MediaType.APPLICATION_JSON.toString());
			return null;
		}
		return formation.getRoot();
	}
}