package fr.univ.amu.pacome.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.user.Composante;
import fr.univ.amu.pacome.repositories.ComposanteRepository;

@Service
public class ComposantesManager {

	@Autowired
	private ComposanteRepository repository;

	public ComposantesManager(){
		
	}
	
	public Composante findOne(Integer idComposante) {
		return repository.findOne(idComposante);
	}

	public List<Composante> findAll() {
		return repository.findAll();
	}

	public Composante save(Composante composante) {
		return repository.save(composante);
	}

	public void delete(Integer idcomposante) {
		repository.delete(idcomposante);
	}

	public List<Composante> findByNameContainingIgnoreCase(String name){
		return repository.findByNameContainingIgnoreCase(name);
	}
}
