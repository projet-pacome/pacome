package fr.univ.amu.pacome.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.user.Nature;
import fr.univ.amu.pacome.repositories.NatureRepository;

@Service
public class NaturesManager {

	@Autowired
	private NatureRepository repository;

	public NaturesManager(){
		
	}
	
	public Nature findOne(Integer idNature) {
		return repository.findOne(idNature);
	}

	public List<Nature> findAll() {
		return repository.findAll();
	}

	public Nature save(Nature nature) {
		return repository.save(nature);
	}

	public void delete(Integer idNature) {
		repository.delete(idNature);
	}
	
	public List<Nature> findByNameContainingIgnoreCase(String name){
		return repository.findByNameContainingIgnoreCase(name);
	}
	
}
