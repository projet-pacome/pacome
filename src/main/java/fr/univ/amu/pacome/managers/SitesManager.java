package fr.univ.amu.pacome.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.tree.Site;
import fr.univ.amu.pacome.repositories.SiteRepository;

@Service
public class SitesManager {

	
	@Autowired
	private SiteRepository repository;
	
	public SitesManager(){
		
	}
	
	public Site findOne(Integer idSite) {
		return repository.findOne(idSite);
	}

	public List<Site> findAll() {
		return repository.findAll();
	}

	public Site save(Site site) {
		return repository.save(site);
	}

	public void delete(Integer idSite) {
		repository.delete(idSite);
	}
	
	public List<Site> findByNameContainingIgnoreCase(String name){
		return repository.findByNameContainingIgnoreCase(name);
	}
	

}
