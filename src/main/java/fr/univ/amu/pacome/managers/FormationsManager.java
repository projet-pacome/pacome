package fr.univ.amu.pacome.managers;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Site;
import fr.univ.amu.pacome.entities.tree.Type;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;
import fr.univ.amu.pacome.repositories.FormationRepository;
import fr.univ.amu.pacome.services.checker.Checker;

@Service
public class FormationsManager {

	@Autowired
	private FormationRepository repository;

	@Autowired
	private NodeManager nm;
	
	@Autowired
	private SitesManager sm;

	@Autowired
	Checker checker;

	public FormationsManager() {

	}

	public Formation findOne(Integer idFormation) {
		return repository.findOne(idFormation);
	}

	public List<Formation> findAll() {
		return repository.findAll();
	}

	/**
	 * Save a formation to set automatically to UNKNOWN after saving or update
	 * 
	 * @param formation
	 * @return saved entity
	 */
	public Formation save(Formation formation) {
		formation.setValidationState(ValidationState.UNKNOWN);
		return repository.save(formation);
	}

	public Formation saveWithoutAlteringValidationState(Formation formation) {
		return repository.save(formation);
	}

	public void delete(Integer idFormation) {
		repository.delete(idFormation);
	}

	public List<Formation> findByNameContainingIgnoreCase(String name) {
		return repository.findByNameContainingIgnoreCase(name);
	}

	public Formation percentageValidation(Formation formation) {
		float sum = formation.getPercentOfCM() + formation.getPercentOfTD() + formation.getPercentOfTP();
		if (sum < 100.0f) {
			float diff = 100.0f - sum;
			float toAdd = diff / 3;
			formation.setPercentOfCM(formation.getPercentOfCM() + toAdd);
			formation.setPercentOfTD(formation.getPercentOfTD() + toAdd);
			formation.setPercentOfTP(formation.getPercentOfTP() + toAdd);
		} else if (sum > 100.0f) {
			float diff = sum - 100.0f;
			float toSub = diff / 3;
			formation.setPercentOfCM(formation.getPercentOfCM() - toSub);
			formation.setPercentOfTD(formation.getPercentOfTD() - toSub);
			formation.setPercentOfTP(formation.getPercentOfTP() - toSub);
		}

		return formation;
	}

	public boolean exist(Formation formation) {
		return formation.getIdFormation() != null;
	}

	public void createFRMNode(Formation formation) {
		Site site = sm.findOne(1);
		Map<Site, Integer> sites = new HashMap<>();
		sites.put(site, 100);
		if( ! exist(formation) ){
			Node frmNode = new Node();
			frmNode.setName(formation.getName());
			frmNode.setCode(formation.getCode());
			frmNode.setType(Type.FRM);
			frmNode.setSites(sites);
			nm.save(frmNode);
			formation.setRoot(frmNode);
		}
	}

	public void validate(Formation formation)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		checker.validate(formation);
	}
}
