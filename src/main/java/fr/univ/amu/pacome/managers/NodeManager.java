package fr.univ.amu.pacome.managers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Type;
import fr.univ.amu.pacome.entities.tree.Utils;
import fr.univ.amu.pacome.repositories.NodeRepository;

@Service
public class NodeManager {

	@Autowired
	private NodeRepository repository;

	public NodeManager() {

	}

	public Node findOne(Integer idNode) {
		return repository.findOne(idNode);
	}

	public List<Node> findAll() {
		return repository.findAll();
	}

	public Node save(Node node) {
		return repository.save(node);
	}

	public void delete(Integer idNode) {
		repository.delete(idNode);
	}

	public List<Node> findByNameContainingIgnoreCase(String name) {
		return repository.findByNameContainingIgnoreCase(name);
	}

	public List<Node> findByCode(String code) {
		return repository.findByCode(code);
	}

	public List<Node> findByType(Type type) {
		return repository.findByType(type);
	}

	public List<Node> searchPotentialNewChildren(Node currentNode){
		List<Type> potentiaType = Utils.getLinkBetweenTwoType().get(Type.PT);

		List<Node> potentialNewChildren = new ArrayList<>();
		System.err.println("Fils potentiel de " + currentNode.getName() + " de type " + currentNode.getType() + " sont : ");
		System.err.println(potentiaType);
		
		for(int i = 0 ; i < potentiaType.size() ; i++){
			potentialNewChildren.addAll(findByType(potentiaType.get(i)));
		}

		for(int i = 0 ; i < potentiaType.size() ; i++){
			System.err.println(potentialNewChildren.get(i).getName());
		}		
		return potentialNewChildren;
	}

	public void deleteCascade(int fatherId, int nodeId) {
		Node fatherNodeToDelete = findOne(fatherId);
		Node nodeToDelete = findOne(nodeId);
		// remove reference to the child in father children
		removeRefOnFatherChildrenBeforeChild(fatherNodeToDelete, nodeToDelete);
		
		if(nodeToDelete.getChildren().isEmpty())
			delete(nodeToDelete.getIdNode());
		else
			browseTreeToDeleteChild(nodeToDelete);
	}
	
	private void removeRefOnFatherChildrenBeforeChild(Node father, Node child) {
		List <Node> chidren = father.getChildren();
		chidren.remove(child);
		save(father);
	}
	
	private void browseTreeToDeleteChild(Node root){
		if(root.getChildren().isEmpty())
			delete(root.getIdNode());
		
		for (Node child : root.getChildren()) {
			if (child.getChildren().isEmpty()){
				removeRefOnFatherChildrenBeforeChild(root, child);
				System.err.println("deleeetttttttt " + child.getName());
				delete(child.getIdNode());
				System.err.println(findOne(child.getIdNode()));
			}
			
			browseTreeToDeleteChild(child);
		}
	}
}
