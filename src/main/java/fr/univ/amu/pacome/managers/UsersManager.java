package fr.univ.amu.pacome.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univ.amu.pacome.entities.user.User;
import fr.univ.amu.pacome.repositories.UserRepository;

@Service
public class UsersManager {

	@Autowired
	private UserRepository repository;

	public UsersManager(){
		
	}
	
	public void login(){
		// TODO : implemente méthod
	}
	
	public void logout (){
		// TODO : implemente méthod
	}
	
	public User findOne(Integer idUser) {
		return repository.findOne(idUser);
	}

	public List<User> findAll() {
		return repository.findAll();
	}

	public User save(User user) {
		return repository.save(user);
	}

	public void delete(Integer idUser) {
		repository.delete(idUser);
	}

	public List<User> findByLastNameContainingIgnoreCase (String name){
		return repository.findByLastNameContainingIgnoreCase(name);
	}
	
	public List<User> findByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase (String lastName, String firstName){
		return repository.findByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase(lastName, firstName);
	}
	
}
