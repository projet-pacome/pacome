package fr.univ.amu.pacome;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Site;
import fr.univ.amu.pacome.entities.tree.Type;
import fr.univ.amu.pacome.entities.user.Composante;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.Nature;
import fr.univ.amu.pacome.entities.user.User;
import fr.univ.amu.pacome.entities.user.ValidationState;
import fr.univ.amu.pacome.managers.ComposantesManager;
import fr.univ.amu.pacome.managers.FormationsManager;
import fr.univ.amu.pacome.managers.NaturesManager;
import fr.univ.amu.pacome.managers.NodeManager;
import fr.univ.amu.pacome.managers.SitesManager;
import fr.univ.amu.pacome.managers.UsersManager;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
public class GenerData {

	@Autowired
	private FormationsManager fm;

	@Autowired
	private NaturesManager nm;

	@Autowired
	private ComposantesManager cm;

	@Autowired
	private UsersManager um;

	@Autowired
	private NodeManager nodeM;

	@Autowired
	private SitesManager sm;

	@Test
	public void insertFormation() {

		Node ue1 = new Node();
		ue1.setType(Type.UE);
		ue1.setName("Génie Logiciel");
		ue1.setCode("UEM1GL");
		ue1.setCredits(6.0f);
		ue1.setHCM(20.0f);
		ue1.setHETD(0.0f);
		ue1.setHNP(0.0f);
		ue1.setHTD(20.0f);
		ue1.setHTP(20.0f);
		ue1.setNbGroupsCM(0);
		ue1.setNbGroupsTD(0);
		ue1.setNbGroupsTP(0);

		Node ue2 = new Node();
		ue2.setType(Type.UE);
		ue2.setName("Compléxité");
		ue2.setCode("UEM1COMPLX");
		ue2.setCredits(6.0f);
		ue2.setHCM(20.0f);
		ue2.setHETD(0.0f);
		ue2.setHNP(0.0f);
		ue2.setHTD(20.0f);
		ue2.setHTP(20.0f);
		ue2.setNbGroupsCM(0);
		ue2.setNbGroupsTD(0);
		ue2.setNbGroupsTP(0);

		Node ue3 = new Node();
		ue3.setType(Type.UE);
		ue3.setName("Réseaux");
		ue3.setCode("UEM1RESX");
		ue3.setCredits(6.0f);
		ue3.setHCM(20.0f);
		ue3.setHETD(0.0f);
		ue3.setHNP(0.0f);
		ue3.setHTD(20.0f);
		ue3.setHTP(20.0f);
		ue3.setNbGroupsCM(0);
		ue3.setNbGroupsTD(0);
		ue3.setNbGroupsTP(0);

		Node ue4 = new Node();
		ue4.setType(Type.UE);
		ue4.setName("Programmation fonctionnelle");
		ue4.setCode("UEM1PF");
		ue4.setCredits(3.0f);
		ue4.setHCM(10.0f);
		ue4.setHETD(0.0f);
		ue4.setHNP(0.0f);
		ue4.setHTD(10.0f);
		ue4.setHTP(10.0f);
		ue4.setNbGroupsCM(0);
		ue4.setNbGroupsTD(0);
		ue4.setNbGroupsTP(0);

		Node ue5 = new Node();
		ue5.setType(Type.UE);
		ue5.setName("BDR : approfondissement");
		ue5.setCode("UEM1BDR");
		ue5.setCredits(3.0f);
		ue5.setHCM(10.0f);
		ue5.setHETD(0.0f);
		ue5.setHNP(0.0f);
		ue5.setHTD(10.0f);
		ue5.setHTP(10.0f);
		ue5.setNbGroupsCM(0);
		ue5.setNbGroupsTD(0);
		ue5.setNbGroupsTP(0);

		Node ue6 = new Node();
		ue6.setType(Type.UE);
		ue6.setName("Système embarqué");
		ue6.setCode("UEM1SE");
		ue6.setCredits(3.0f);
		ue6.setHCM(10.0f);
		ue6.setHETD(0.0f);
		ue6.setHNP(0.0f);
		ue6.setHTD(10.0f);
		ue6.setHTP(10.0f);
		ue6.setNbGroupsCM(0);
		ue6.setNbGroupsTD(0);
		ue6.setNbGroupsTP(0);

		Node ue7 = new Node();
		ue7.setType(Type.UE);
		ue7.setName("Modèles à évènements discrets");
		ue7.setCode("UEM1MED");
		ue7.setCredits(3.0f);
		ue7.setHCM(20.0f);
		ue7.setHETD(0.0f);
		ue7.setHNP(0.0f);
		ue7.setHTD(10.0f);
		ue7.setHTP(0.0f);
		ue7.setNbGroupsCM(0);
		ue7.setNbGroupsTD(0);
		ue7.setNbGroupsTP(0);

		Node ue8 = new Node();
		ue8.setType(Type.UE);
		ue8.setName("Anglais");
		ue8.setCode("UEM1ANG");
		ue8.setCredits(3.0f);
		ue8.setHCM(10.0f);
		ue8.setHETD(0.0f);
		ue8.setHNP(0.0f);
		ue8.setHTD(10.0f);
		ue8.setHTP(10.0f);
		ue8.setNbGroupsCM(0);
		ue8.setNbGroupsTD(0);
		ue8.setNbGroupsTP(0);

		Node ue9 = new Node();
		ue9.setType(Type.UE);
		ue9.setName("Communication");
		ue9.setCode("UEM1COM");
		ue9.setCredits(3.0f);
		ue9.setHCM(15.0f);
		ue9.setHETD(0.0f);
		ue9.setHNP(0.0f);
		ue9.setHTD(8.0f);
		ue9.setHTP(0.0f);
		ue9.setNbGroupsCM(0);
		ue9.setNbGroupsTD(0);
		ue9.setNbGroupsTP(0);

		Node ue10 = new Node();
		ue10.setType(Type.UE);
		ue10.setName("XML");
		ue10.setCode("UEM1XML");
		ue10.setCredits(3.0f);
		ue10.setHCM(10.0f);
		ue10.setHETD(0.0f);
		ue10.setHNP(0.0f);
		ue10.setHTD(10.0f);
		ue10.setHTP(10.0f);
		ue10.setNbGroupsCM(0);
		ue10.setNbGroupsTD(0);
		ue10.setNbGroupsTP(0);

		Node ue11 = new Node();
		ue11.setType(Type.UE);
		ue11.setName("Algorithmique et recherche opérationnelle");
		ue11.setCode("UEM1ARO");
		ue11.setCredits(3.0f);
		ue11.setHCM(10.0f);
		ue11.setHETD(0.0f);
		ue11.setHNP(0.0f);
		ue11.setHTD(10.0f);
		ue11.setHTP(10.0f);
		ue11.setNbGroupsCM(0);
		ue11.setNbGroupsTD(0);
		ue11.setNbGroupsTP(0);

		Node ue12 = new Node();
		ue12.setType(Type.UE);
		ue12.setName("Programmation parallèle");
		ue12.setCode("UEM1PP");
		ue12.setCredits(3.0f);
		ue12.setHCM(10.0f);
		ue12.setHETD(0.0f);
		ue12.setHNP(0.0f);
		ue12.setHTD(10.0f);
		ue12.setHTP(10.0f);
		ue12.setNbGroupsCM(0);
		ue12.setNbGroupsTD(0);
		ue12.setNbGroupsTP(0);

		Node ue13 = new Node();
		ue13.setType(Type.UE);
		ue13.setName("Projet");
		ue13.setCode("UEM1PRJ");
		ue13.setCredits(6.0f);
		ue13.setHCM(0.0f);
		ue13.setHETD(0.0f);
		ue13.setHNP(0.0f);
		ue13.setHTD(0.0f);
		ue13.setHTP(0.0f);
		ue13.setNbGroupsCM(0);
		ue13.setNbGroupsTD(0);
		ue13.setNbGroupsTP(0);

		Node ue14 = new Node();
		ue14.setType(Type.UE);
		ue14.setName("Intelligence artificielle");
		ue14.setCode("UEM1IA");
		ue14.setCredits(3.0f);
		ue14.setHCM(10.0f);
		ue14.setHETD(0.0f);
		ue14.setHNP(0.0f);
		ue14.setHTD(10.0f);
		ue14.setHTP(10.0f);
		ue14.setNbGroupsCM(0);
		ue14.setNbGroupsTD(0);
		ue14.setNbGroupsTP(0);

		Node ue15 = new Node();
		ue15.setType(Type.UE);
		ue15.setName("Algorithmique distribuée");
		ue15.setCode("UEM1AD");
		ue15.setCredits(3.0f);
		ue15.setHCM(10.0f);
		ue15.setHETD(0.0f);
		ue15.setHNP(0.0f);
		ue15.setHTD(10.0f);
		ue15.setHTP(10.0f);
		ue15.setNbGroupsCM(0);
		ue15.setNbGroupsTD(0);
		ue15.setNbGroupsTP(0);

		Node ue16 = new Node();
		ue16.setType(Type.UE);
		ue16.setName("Introduction à l'apprentissage automatique");
		ue16.setCode("UEM1IAA");
		ue16.setCredits(3.0f);
		ue16.setHCM(10.0f);
		ue16.setHETD(0.0f);
		ue16.setHNP(0.0f);
		ue16.setHTD(10.0f);
		ue16.setHTP(10.0f);
		ue16.setNbGroupsCM(0);
		ue16.setNbGroupsTD(0);
		ue16.setNbGroupsTP(0);

		Node ue17 = new Node();
		ue17.setType(Type.UE);
		ue17.setName("Anglais 2");
		ue17.setCode("UEM2FSILAN2");
		ue17.setCredits(3.0f);
		ue17.setHCM(15.0f);
		ue17.setHETD(0.0f);
		ue17.setHNP(0.0f);
		ue17.setHTD(15.0f);
		ue17.setHTP(0.0f);
		ue17.setNbGroupsCM(0);
		ue17.setNbGroupsTD(0);
		ue17.setNbGroupsTP(0);

		Node ue18 = new Node();
		ue18.setType(Type.UE);
		ue18.setName("Communication 2");
		ue18.setCode("UEM2FSILCOM2");
		ue18.setCredits(3.0f);
		ue18.setHCM(15.0f);
		ue18.setHETD(0.0f);
		ue18.setHNP(0.0f);
		ue18.setHTD(8.0f);
		ue18.setHTP(0.0f);
		ue18.setNbGroupsCM(0);
		ue18.setNbGroupsTD(0);
		ue18.setNbGroupsTP(0);

		Node ue19 = new Node();
		ue19.setType(Type.UE);
		ue19.setName("Fiabilité logicielle 1");
		ue19.setCode("UEM2FSILFL");
		ue19.setCredits(3.0f);
		ue19.setHCM(10.0f);
		ue19.setHETD(0.0f);
		ue19.setHNP(0.0f);
		ue19.setHTD(10.0f);
		ue19.setHTP(10.0f);
		ue19.setNbGroupsCM(0);
		ue19.setNbGroupsTD(0);
		ue19.setNbGroupsTP(0);

		Node ue20 = new Node();
		ue20.setType(Type.UE);
		ue20.setName("Plateforme JEE");
		ue20.setCode("UEM2FSILPJ");
		ue20.setCredits(3.0f);
		ue20.setHCM(12.0f);
		ue20.setHETD(0.0f);
		ue20.setHNP(0.0f);
		ue20.setHTD(0.0f);
		ue20.setHTP(18.0f);
		ue20.setNbGroupsCM(0);
		ue20.setNbGroupsTD(0);
		ue20.setNbGroupsTP(0);

		Node ue21 = new Node();
		ue21.setType(Type.UE);
		ue21.setName("Systèmes hétérogènes");
		ue21.setCode("UEM2FSILSH");
		ue21.setCredits(3.0f);
		ue21.setHCM(12.0f);
		ue21.setHETD(0.0f);
		ue21.setHNP(0.0f);
		ue21.setHTD(10.0f);
		ue21.setHTP(8.0f);
		ue21.setNbGroupsCM(0);
		ue21.setNbGroupsTD(0);
		ue21.setNbGroupsTP(0);

		Node ue22 = new Node();
		ue22.setType(Type.UE);
		ue22.setName("Administration système et réseau");
		ue22.setCode("UEM2ISLASR");
		ue22.setCredits(3.0f);
		ue22.setHCM(12.0f);
		ue22.setHETD(0.0f);
		ue22.setHNP(0.0f);
		ue22.setHTD(10.0f);
		ue22.setHTP(18.0f);
		ue22.setNbGroupsCM(0);
		ue22.setNbGroupsTD(0);
		ue22.setNbGroupsTP(0);

		Node ue23 = new Node();
		ue23.setType(Type.UE);
		ue23.setName("Méthodes de développement");
		ue23.setCode("UEM2ISLMD");
		ue23.setCredits(3.0f);
		ue23.setHCM(12.0f);
		ue23.setHETD(0.0f);
		ue23.setHNP(0.0f);
		ue23.setHTD(10.0f);
		ue23.setHTP(18.0f);
		ue23.setNbGroupsCM(0);
		ue23.setNbGroupsTD(0);
		ue23.setNbGroupsTP(0);

		Node ue24 = new Node();
		ue24.setType(Type.UE);
		ue24.setName("Architecture logicielle JEE");
		ue24.setCode("UEM2ISLALJ");
		ue24.setCredits(3.0f);
		ue24.setHCM(6.0f);
		ue24.setHETD(0.0f);
		ue24.setHNP(0.0f);
		ue24.setHTD(0.0f);
		ue24.setHTP(24.0f);
		ue24.setNbGroupsCM(0);
		ue24.setNbGroupsTD(0);
		ue24.setNbGroupsTP(0);

		Node ue25 = new Node();
		ue25.setType(Type.UE);
		ue25.setName("Intégration des données");
		ue25.setCode("UEM2ISLID");
		ue25.setCredits(3.0f);
		ue25.setHCM(15.0f);
		ue25.setHETD(0.0f);
		ue25.setHNP(0.0f);
		ue25.setHTD(0.0f);
		ue25.setHTP(15.0f);
		ue25.setNbGroupsCM(0);
		ue25.setNbGroupsTD(0);
		ue25.setNbGroupsTP(0);

		Node ue26 = new Node();
		ue26.setType(Type.UE);
		ue26.setName("Administration et alimentation de BD");
		ue26.setCode("UEM2ISLAABD");
		ue26.setCredits(3.0f);
		ue26.setHCM(15.0f);
		ue26.setHETD(0.0f);
		ue26.setHNP(0.0f);
		ue26.setHTD(0.0f);
		ue26.setHTP(15.0f);
		ue26.setNbGroupsCM(0);
		ue26.setNbGroupsTD(0);
		ue26.setNbGroupsTP(0);

		Node ue27 = new Node();
		ue27.setType(Type.UE);
		ue27.setName("Projet industriel de mise en application");
		ue27.setCode("M2ISLPIMA");
		ue27.setCredits(6.0f);
		ue27.setHCM(0.0f);
		ue27.setHETD(0.0f);
		ue27.setHNP(0.0f);
		ue27.setHTD(0.0f);
		ue27.setHTP(0.0f);
		ue27.setNbGroupsCM(0);
		ue27.setNbGroupsTD(0);
		ue27.setNbGroupsTP(0);

		Node ue28 = new Node();
		ue28.setType(Type.UE);
		ue28.setName("Stage en entreprise");
		ue28.setCode("M2ISLSE");
		ue28.setCredits(24.0f);
		ue28.setHCM(0.0f);
		ue28.setHETD(0.0f);
		ue28.setHNP(0.0f);
		ue28.setHTD(0.0f);
		ue28.setHTP(0.0f);
		ue28.setNbGroupsCM(0);
		ue28.setNbGroupsTD(0);
		ue28.setNbGroupsTP(0);

		Node LI = new Node();
		LI.setType(Type.LI);
		LI.setName("TC des parcours FSI et ISL");
		LI.setCode("M2FSILTC");
		LI.setCredits(18.0f);
		LI.setHCM(0.0f);
		LI.setHETD(0.0f);
		LI.setHNP(0.0f);
		LI.setHTD(0.0f);
		LI.setHTP(0.0f);
		LI.setNbGroupsCM(0);
		LI.setNbGroupsTD(0);
		LI.setNbGroupsTP(0);
		LI.setChildren(Arrays.asList(ue17, ue18, ue19, ue20, ue21));

		Node semestre1 = new Node();
		semestre1.setType(Type.SE);
		semestre1.setName("Semestre 1");
		semestre1.setCode("SE1M1");
		semestre1.setCredits(30.0f);
		semestre1.setHCM(0.0f);
		semestre1.setHETD(0.0f);
		semestre1.setHNP(0.0f);
		semestre1.setHTD(0.0f);
		semestre1.setHTP(0.0f);
		semestre1.setNbGroupsCM(0);
		semestre1.setNbGroupsTD(0);
		semestre1.setNbGroupsTP(0);
		semestre1.setChildren(Arrays.asList(ue1, ue2, ue3, ue4, ue5, ue6, ue7));

		Node semestre2 = new Node();
		semestre2.setType(Type.SE);
		semestre2.setName("Semestre 2");
		semestre2.setCode("SE2M1");
		semestre2.setCredits(30.0f);
		semestre2.setHCM(0.0f);
		semestre2.setHETD(0.0f);
		semestre2.setHNP(0.0f);
		semestre2.setHTD(0.0f);
		semestre2.setHTP(0.0f);
		semestre2.setNbGroupsCM(0);
		semestre2.setNbGroupsTD(0);
		semestre2.setNbGroupsTP(0);
		semestre2.setChildren(Arrays.asList(ue8, ue9, ue10, ue11, ue12, ue13, ue14, ue15, ue16));

		Node semestre3 = new Node();
		semestre3.setType(Type.SE);
		semestre3.setName("Semestre 3");
		semestre3.setCode("SE3M2ISL");
		semestre3.setCredits(30.0f);
		semestre3.setHCM(0.0f);
		semestre3.setHETD(0.0f);
		semestre3.setHNP(0.0f);
		semestre3.setHTD(0.0f);
		semestre3.setHTP(0.0f);
		semestre3.setNbGroupsCM(0);
		semestre3.setNbGroupsTD(0);
		semestre3.setNbGroupsTP(0);
		semestre3.setChildren(Arrays.asList(LI, ue22, ue23, ue24, ue25, ue26));

		Node semestre4 = new Node();
		semestre4.setType(Type.SE);
		semestre4.setName("Semestre 4");
		semestre4.setCode("SE4M2ISL");
		semestre4.setCredits(30.0f);
		semestre4.setHCM(0.0f);
		semestre4.setHETD(0.0f);
		semestre4.setHNP(0.0f);
		semestre4.setHTD(0.0f);
		semestre4.setHTP(0.0f);
		semestre4.setNbGroupsCM(0);
		semestre4.setNbGroupsTD(0);
		semestre4.setNbGroupsTP(0);
		semestre4.setChildren(Arrays.asList(ue27, ue28));

		Node annee1 = new Node();
		annee1.setType(Type.AN);
		annee1.setName("Annee 1");
		annee1.setCode("AN1M1");
		annee1.setCredits(60.0f);
		annee1.setHCM(0.0f);
		annee1.setHETD(0.0f);
		annee1.setHNP(0.0f);
		annee1.setHTD(0.0f);
		annee1.setHTP(0.0f);
		annee1.setNbGroupsCM(0);
		annee1.setNbGroupsTD(0);
		annee1.setNbGroupsTP(0);
		annee1.setChildren(Arrays.asList(semestre1, semestre2));

		Node annee2 = new Node();
		annee2.setType(Type.AN);
		annee2.setName("Annee 2");
		annee2.setCode("AN2M2ISL");
		annee2.setCredits(60.0f);
		annee2.setHCM(0.0f);
		annee2.setHETD(0.0f);
		annee2.setHNP(0.0f);
		annee2.setHTD(0.0f);
		annee2.setHTP(0.0f);
		annee2.setNbGroupsCM(0);
		annee2.setNbGroupsTD(0);
		annee2.setNbGroupsTP(0);
		annee2.setChildren(Arrays.asList(semestre3, semestre4));

		Node parcours = new Node();
		parcours.setType(Type.PT);
		parcours.setName("Parcours Intégration de systèmes logiciels (ISL)");
		parcours.setCode("PTISL");
		parcours.setCredits(120.0f);
		parcours.setHCM(0.0f);
		parcours.setHETD(0.0f);
		parcours.setHNP(0.0f);
		parcours.setHTD(0.0f);
		parcours.setHTP(0.0f);
		parcours.setNbGroupsCM(0);
		parcours.setNbGroupsTD(0);
		parcours.setNbGroupsTP(0);
		parcours.setChildren(Arrays.asList(annee1, annee2));

		Node frm = new Node();
		frm.setType(Type.FRM);
		frm.setName("Master Informatique");
		frm.setCode("MINFO");
		frm.setCredits(0.0f);
		frm.setHCM(0.0f);
		frm.setHETD(0.0f);
		frm.setHETDComputed(0.0f);
		frm.setHNP(0.0f);
		frm.setHTD(0.0f);
		frm.setHTP(0.0f);
		frm.setNbGroupsCM(0);
		frm.setNbGroupsTD(0);
		frm.setNbGroupsTP(0);
		frm.setChildren(Arrays.asList(parcours));

		Composante composante = new Composante();
		composante.setCode("UFRSCIENCES");
		composante.setName("UFR sciences");
		composante.setConversionRateTPToHETD(0.75f);

		Nature nat1 = new Nature();
		nat1.setName("Master");
		Nature nat2 = new Nature();
		nat2.setName("Licence");
		Nature nat3 = new Nature();
		nat3.setName("DUT");
		Nature nat4 = new Nature();
		nat4.setName("Doctorat");

		Site site = new Site();
		site.setName("UFR Science Luminy");
		sm.save(site);
		Map<Site, Integer> sites = new HashMap<>();
		sites.put(site, 100);

		Formation formation = new Formation();
		formation.setCMGroupSize(30);
		formation.setTDGroupSize(20);
		formation.setTPGroupSize(15);
		formation.setCode("MASTER01");
		formation.setName("Informatique");
		formation.setNature(nat1);
		formation.setNumberOfHoursPerCredit(10.0f);
		formation.setPercentOfCM(30.0f);
		formation.setPercentOfTD(30.0f);
		formation.setPercentOfTP(40.0f);
		formation.setActivationState(true);
		formation.setValidationState(ValidationState.UNKNOWN);
		formation.setComposante(composante);
		formation.setRoot(frm);

		frm.setSites(sites);

		User user = new User();
		user.setFirstName("Michel");
		user.setLastName("OBAMA");
		user.setComposante(composante);

		nm.save(nat1);
		nm.save(nat2);
		nm.save(nat3);
		nm.save(nat4);

		cm.save(composante);
		um.save(user);

		nodeM.save(ue1);
		nodeM.save(ue2);
		nodeM.save(ue3);
		nodeM.save(ue4);
		nodeM.save(ue5);
		nodeM.save(ue6);
		nodeM.save(ue7);
		nodeM.save(ue8);
		nodeM.save(ue9);
		nodeM.save(ue10);
		nodeM.save(ue11);
		nodeM.save(ue12);
		nodeM.save(ue13);
		nodeM.save(ue14);
		nodeM.save(ue15);
		nodeM.save(ue16);

		nodeM.save(ue17);
		nodeM.save(ue18);
		nodeM.save(ue19);
		nodeM.save(ue20);
		nodeM.save(ue21);
		nodeM.save(ue22);
		nodeM.save(ue23);
		nodeM.save(ue24);
		nodeM.save(ue25);
		nodeM.save(ue26);
		nodeM.save(ue27);
		nodeM.save(ue28);

		nodeM.save(LI);

		nodeM.save(semestre1);
		nodeM.save(semestre2);
		nodeM.save(semestre3);
		nodeM.save(semestre4);

		nodeM.save(annee1);
		nodeM.save(annee2);
		nodeM.save(parcours);
		nodeM.save(frm);

		fm.saveWithoutAlteringValidationState(formation);

		User userFormation = new User();
		Set<Formation> formations = new HashSet<>();
		formations.add(formation);
		userFormation.setFormations(formations);
		userFormation.setFirstName("Gérard");
		userFormation.setLastName("Elimelek");

		Set<User> users = new HashSet<>();
		users.add(userFormation);
		formation.setUsers(users);

		Set<Node> lesNodes = new HashSet<Node>();
		lesNodes.add(ue1);
		lesNodes.add(ue2);
		lesNodes.add(ue3);
		lesNodes.add(ue4);
		lesNodes.add(ue5);
		lesNodes.add(ue6);
		lesNodes.add(ue7);
		lesNodes.add(ue8);
		lesNodes.add(ue9);
		lesNodes.add(ue10);
		lesNodes.add(ue11);
		lesNodes.add(ue12);
		lesNodes.add(ue13);
		lesNodes.add(ue14);
		lesNodes.add(ue15);
		lesNodes.add(ue16);
		lesNodes.add(ue17);
		lesNodes.add(ue18);
		lesNodes.add(ue19);
		lesNodes.add(ue20);
		lesNodes.add(ue21);
		lesNodes.add(ue22);
		lesNodes.add(ue23);
		lesNodes.add(ue24);
		lesNodes.add(ue25);
		lesNodes.add(ue26);
		lesNodes.add(ue27);
		lesNodes.add(ue28);

		lesNodes.add(LI);

		lesNodes.add(semestre1);
		lesNodes.add(semestre2);
		lesNodes.add(semestre3);
		lesNodes.add(semestre4);

		lesNodes.add(annee1);
		lesNodes.add(annee2);

		lesNodes.add(parcours);
		lesNodes.add(frm);

		um.save(userFormation);

		fm.saveWithoutAlteringValidationState(formation);

		// ------------------------------------------------------Invalid
		// formation

		Set<Node> lesNodesInvalid = new HashSet<Node>();

		Node ec2 = new Node();
		ec2.setType(Type.EC);
		ec2.setName("CIR");
		ec2.setCode("ECCIR");
		ec2.setCredits(6.0f);
		ec2.setHCM(20.0f);
		ec2.setHETD(0.0f);
		ec2.setHETDComputed(0.0f);
		ec2.setHNP(0.0f);
		ec2.setHTD(20.0f);
		ec2.setHTP(20.0f);
		ec2.setNbGroupsCM(0);
		ec2.setNbGroupsTD(0);
		ec2.setNbGroupsTP(0);

		Node frm1 = new Node();
		frm1.setType(Type.FRM);
		frm1.setName("Master ");
		frm1.setCode("MIPINFO");
		frm1.setCredits(0.0f);
		frm1.setHCM(0.0f);
		frm1.setHETD(0.0f);
		frm1.setHETDComputed(0.0f);
		frm1.setHNP(0.0f);
		frm1.setHTD(0.0f);
		frm1.setHTP(0.0f);
		frm1.setNbGroupsCM(0);
		frm1.setNbGroupsTD(0);
		frm1.setNbGroupsTP(0);
		frm1.setChildren(Arrays.asList(ec2));

		Node OP = new Node();
		OP.setType(Type.OP);
		OP.setName("Invalid OP");
		OP.setCode("OP54");
		OP.setCredits(18.0f);
		OP.setHCM(0.0f);
		OP.setHETD(0.0f);
		OP.setHETDComputed(0.0f);
		OP.setHNP(0.0f);
		OP.setHTD(0.0f);
		OP.setHTP(0.0f);
		OP.setNbGroupsCM(0);
		OP.setNbGroupsTD(0);
		OP.setNbGroupsTP(0);
		OP.setChildren(Arrays.asList(frm1));

		Node parcours1 = new Node();
		parcours1.setType(Type.PT);
		parcours1.setName("Parcours Intégration de systèmes logiciels (ISL)");
		parcours1.setCode("PTSS5");
		parcours1.setCredits(120.0f);
		parcours1.setHCM(0.0f);
		parcours1.setHETD(0.0f);
		parcours1.setHETDComputed(0.0f);
		parcours1.setHNP(0.0f);
		parcours1.setHTD(0.0f);
		parcours1.setHTP(0.0f);
		parcours1.setNbGroupsCM(0);
		parcours1.setNbGroupsTD(0);
		parcours1.setNbGroupsTP(0);
		parcours1.setChildren(Arrays.asList(OP));

		Node LI2 = new Node();
		LI2.setType(Type.LI);
		LI2.setName("Invalid LI");
		LI2.setCode("LI262");
		LI2.setCredits(18.0f);
		LI2.setHCM(0.0f);
		LI2.setHETD(0.0f);
		LI2.setHETDComputed(0.0f);
		LI2.setHNP(0.0f);
		LI2.setHTD(0.0f);
		LI2.setHTP(0.0f);
		LI2.setNbGroupsCM(0);
		LI2.setNbGroupsTD(0);
		LI2.setNbGroupsTP(0);
		LI2.setChildren(Arrays.asList(parcours1));

		Node annee3 = new Node();
		annee3.setType(Type.AN);
		annee3.setName("Annee 1");
		annee3.setCode("AN4A121");
		annee3.setCredits(60.0f);
		annee3.setHCM(0.0f);
		annee3.setHETD(0.0f);
		annee3.setHETDComputed(0.0f);
		annee3.setHNP(0.0f);
		annee3.setHTD(0.0f);
		annee3.setHTP(0.0f);
		annee3.setNbGroupsCM(0);
		annee3.setNbGroupsTD(0);
		annee3.setNbGroupsTP(0);
		annee3.setChildren(Arrays.asList(LI2));

		Node semestre7 = new Node();
		semestre7.setType(Type.SE);
		semestre7.setName("Semestre 3");
		semestre7.setCode("SE123");
		semestre7.setCredits(30.0f);
		semestre7.setHCM(0.0f);
		semestre7.setHETD(0.0f);
		semestre7.setHETDComputed(0.0f);
		semestre7.setHNP(0.0f);
		semestre7.setHTD(0.0f);
		semestre7.setHTP(0.0f);
		semestre7.setNbGroupsCM(0);
		semestre7.setNbGroupsTD(0);
		semestre7.setNbGroupsTP(0);
		semestre7.setChildren(Arrays.asList(annee3));

		Node ue29 = new Node();
		ue29.setType(Type.UE);
		ue29.setName("Génie Logiciel");
		ue29.setCode("UEJOJO");
		ue29.setCredits(6.0f);
		ue29.setHCM(20.0f);
		ue29.setHETD(0.0f);
		ue29.setHETDComputed(0.0f);
		ue29.setHNP(0.0f);
		ue29.setHTD(20.0f);
		ue29.setHTP(20.0f);
		ue29.setNbGroupsCM(0);
		ue29.setNbGroupsTD(0);
		ue29.setNbGroupsTP(0);
		ue29.setChildren(Arrays.asList(semestre7));

		Node ec1 = new Node();
		ec1.setType(Type.EC);
		ec1.setName("Architecture JEE");
		ec1.setCode("ECJEE");
		ec1.setCredits(6.0f);
		ec1.setHCM(20.0f);
		ec1.setHETD(0.0f);
		ec1.setHNP(0.0f);
		ec1.setHETDComputed(0.0f);
		ec1.setHTD(20.0f);
		ec1.setHTP(20.0f);
		ec1.setNbGroupsCM(0);
		ec1.setNbGroupsTD(0);
		ec1.setNbGroupsTP(0);
		ec1.setChildren(Arrays.asList(ue29));

		nodeM.save(ec2);
		nodeM.save(frm1);
		nodeM.save(OP);
		nodeM.save(parcours1);
		nodeM.save(LI2);
		nodeM.save(annee3);
		nodeM.save(semestre7);
		nodeM.save(ue29);
		nodeM.save(ec1);

		lesNodesInvalid.add(frm1);
		lesNodesInvalid.add(parcours1);
		lesNodesInvalid.add(annee3);
		lesNodesInvalid.add(semestre7);
		lesNodesInvalid.add(ue29);
		lesNodesInvalid.add(ec1);

		Formation invalidFormation = new Formation();
		invalidFormation.setCMGroupSize(30);
		invalidFormation.setTDGroupSize(20);
		invalidFormation.setTPGroupSize(15);
		invalidFormation.setCode("MASTER30");
		invalidFormation.setName("arbre dèsorganiser");
		invalidFormation.setNature(nat1);
		invalidFormation.setNumberOfHoursPerCredit(10.0f);
		invalidFormation.setPercentOfCM(30.0f);
		invalidFormation.setPercentOfTD(30.0f);
		invalidFormation.setPercentOfTP(40.0f);
		invalidFormation.setActivationState(true);
		invalidFormation.setValidationState(ValidationState.UNKNOWN);
		invalidFormation.setComposante(composante);
		invalidFormation.setRoot(ec1);

		User userInvalidFormation = new User();
		Set<Formation> Invalidformations = new HashSet<>();
		Invalidformations.add(invalidFormation);
		userInvalidFormation.setFormations(Invalidformations);
		userInvalidFormation.setFirstName("Zayd");
		userInvalidFormation.setLastName("HACHCHAM");

		Set<User> iusers = new HashSet<>();
		users.add(userInvalidFormation);
		formation.setUsers(iusers);

		invalidFormation.setNodes(lesNodes);
		invalidFormation.setUsers(iusers);

		fm.saveWithoutAlteringValidationState(invalidFormation);

		/*------------------------------------*/
		/*--------Formation invalide----------*/
		/*------------------------------------*/
		Node ueTest = new Node();
		ueTest.setType(Type.UE);
		ueTest.setName("UE de test");
		ueTest.setCode("UEM1TEST");
		ueTest.setCredits(6.0f);
		ueTest.setNbGroupsCM(0);
		ueTest.setNbGroupsTD(0);
		ueTest.setNbGroupsTP(0);

		Node semestreTest = new Node();
		semestreTest.setType(Type.SE);
		semestreTest.setName("Semestre Test");
		semestreTest.setCode("SE2M1TEST");
		semestreTest.setCredits(30.0f);
		semestreTest.setNbGroupsCM(0);
		semestreTest.setNbGroupsTD(0);
		semestreTest.setNbGroupsTP(0);
		semestreTest.setChildren(Arrays.asList(ueTest));

		Node anneeTest = new Node();
		anneeTest.setType(Type.AN);
		anneeTest.setName("Annee Test");
		anneeTest.setCode("AN1M1TEST");
		anneeTest.setCredits(60.0f);
		anneeTest.setNbGroupsCM(0);
		anneeTest.setNbGroupsTD(0);
		anneeTest.setNbGroupsTP(0);
		anneeTest.setChildren(Arrays.asList(semestreTest));

		Node parcoursTest = new Node();
		parcoursTest.setType(Type.PT);
		parcoursTest.setName("Parcours Test");
		parcoursTest.setCode("PTISLTEST");
		parcoursTest.setCredits(120.0f);
		parcoursTest.setNbGroupsCM(0);
		parcoursTest.setNbGroupsTD(0);
		parcoursTest.setNbGroupsTP(0);
		parcoursTest.setChildren(Arrays.asList(anneeTest));

		Node frmTest = new Node();
		frmTest.setType(Type.FRM);
		frmTest.setName("Master Informatique Test");
		frmTest.setCode("MINFOTEST");
		frmTest.setCredits(0.0f);
		frmTest.setNbGroupsCM(0);
		frmTest.setNbGroupsTD(0);
		frmTest.setNbGroupsTP(0);
		frmTest.setChildren(Arrays.asList(parcoursTest));

		Formation formationTest = new Formation();
		formationTest.setCMGroupSize(30);
		formationTest.setTDGroupSize(20);
		formationTest.setTPGroupSize(15);
		formationTest.setCode("MASTERINVALIDE");
		formationTest.setName("formation invalide");
		formationTest.setNature(nat1);
		formationTest.setNumberOfHoursPerCredit(10.0f);
		formationTest.setPercentOfCM(20.0f);
		formationTest.setPercentOfTD(20.0f);
		formationTest.setPercentOfTP(60.0f);
		formationTest.setActivationState(true);
		formationTest.setValidationState(ValidationState.UNKNOWN);
		formationTest.setComposante(composante);
		formationTest.setRoot(frmTest);

		nodeM.save(ueTest);
		nodeM.save(semestreTest);
		nodeM.save(anneeTest);
		nodeM.save(parcoursTest);
		frmTest.setSites(sites);
		nodeM.save(frmTest);
		fm.saveWithoutAlteringValidationState(formationTest);
		formationTest.setUsers(users);

		/*-----------------------------------------------*/
		/*--------Formation without HCM/HTD/HTP----------*/
		/*-----------------------------------------------*/
		Node ue1ID = new Node();
		ue1ID.setType(Type.UE);
		ue1ID.setName("UE 1 de ID");
		ue1ID.setCode("UE1M1ID");
		ue1ID.setCredits(6.0f);
		ue1ID.setHETD(0.0f);
		ue1ID.setHCM(0.0f);
		ue1ID.setHTD(0.0f);
		ue1ID.setHTP(0.0f);
		ue1ID.setHNP(0.0f);
		ue1ID.setNbGroupsCM(0);
		ue1ID.setNbGroupsTD(0);
		ue1ID.setNbGroupsTP(0);

		Node ue2ID = new Node();
		ue2ID.setType(Type.UE);
		ue2ID.setName("UE 2 de ID");
		ue2ID.setCode("UE2M1ID");
		ue2ID.setCredits(6.0f);
		ue2ID.setHETD(0.0f);
		ue2ID.setHCM(0.0f);
		ue2ID.setHTD(0.0f);
		ue2ID.setHTP(0.0f);
		ue2ID.setHNP(0.0f);
		ue2ID.setNbGroupsCM(0);
		ue2ID.setNbGroupsTD(0);
		ue2ID.setNbGroupsTP(0);

		Node semestre1ID = new Node();
		semestre1ID.setType(Type.SE);
		semestre1ID.setName("Semestre 1 ID");
		semestre1ID.setCode("SE1M1ID");
		semestre1ID.setCredits(30.0f);
		semestre1ID.setHETD(0.0f);
		semestre1ID.setHCM(0.0f);
		semestre1ID.setHTD(0.0f);
		semestre1ID.setHTP(0.0f);
		semestre1ID.setHNP(0.0f);
		semestre1ID.setNbGroupsCM(0);
		semestre1ID.setNbGroupsTD(0);
		semestre1ID.setNbGroupsTP(0);
		semestre1ID.setChildren(Arrays.asList(ue1ID));

		Node semestre2ID = new Node();
		semestre2ID.setType(Type.SE);
		semestre2ID.setName("Semestre 2 ID");
		semestre2ID.setCode("SE2M1ID");
		semestre2ID.setCredits(30.0f);
		semestre2ID.setHETD(0.0f);
		semestre2ID.setHCM(0.0f);
		semestre2ID.setHTD(0.0f);
		semestre2ID.setHTP(0.0f);
		semestre2ID.setHNP(0.0f);
		semestre2ID.setNbGroupsCM(0);
		semestre2ID.setNbGroupsTD(0);
		semestre2ID.setNbGroupsTP(0);
		semestre2ID.setChildren(Arrays.asList(ue2ID));

		Node anneeID = new Node();
		anneeID.setType(Type.AN);
		anneeID.setName("Annee ID");
		anneeID.setCode("AN1M1ID");
		anneeID.setCredits(60.0f);
		anneeID.setHETD(0.0f);
		anneeID.setHCM(0.0f);
		anneeID.setHTD(0.0f);
		anneeID.setHTP(0.0f);
		anneeID.setHNP(0.0f);
		anneeID.setNbGroupsCM(0);
		anneeID.setNbGroupsTD(0);
		anneeID.setNbGroupsTP(0);
		anneeID.setChildren(Arrays.asList(semestre1ID, semestre2ID));

		Node parcoursID = new Node();
		parcoursID.setType(Type.PT);
		parcoursID.setName("Parcours ID");
		parcoursID.setCode("PTID");
		parcoursID.setCredits(120.0f);
		parcoursID.setHETD(0.0f);
		parcoursID.setHCM(0.0f);
		parcoursID.setHTD(0.0f);
		parcoursID.setHTP(0.0f);
		parcoursID.setHNP(0.0f);
		parcoursID.setNbGroupsCM(0);
		parcoursID.setNbGroupsTD(0);
		parcoursID.setNbGroupsTP(0);
		parcoursID.setChildren(Arrays.asList(anneeID));

		Node frmID = new Node();
		frmID.setType(Type.FRM);
		frmID.setName("Master Informatique id");
		frmID.setCode("MINFOID");
		frmID.setCredits(0.0f);
		frmID.setHETD(0.0f);
		frmID.setHCM(0.0f);
		frmID.setHTD(0.0f);
		frmID.setHTP(0.0f);
		frmID.setHNP(0.0f);
		frmID.setNbGroupsCM(0);
		frmID.setNbGroupsTD(0);
		frmID.setNbGroupsTP(0);
		frmID.setChildren(Arrays.asList(parcoursID));

		Formation formationID = new Formation();
		formationID.setCMGroupSize(30);
		formationID.setTDGroupSize(20);
		formationID.setTPGroupSize(15);
		formationID.setCode("MASTER01ID");
		formationID.setName("Informatique ID");
		formationID.setNature(nat1);
		formationID.setNumberOfHoursPerCredit(10.0f);
		formationID.setPercentOfCM(20.0f);
		formationID.setPercentOfTD(20.0f);
		formationID.setPercentOfTP(60.0f);
		formationID.setActivationState(true);
		formationID.setValidationState(ValidationState.UNKNOWN);
		formationID.setComposante(composante);
		formationID.setRoot(frmID);

		nodeM.save(ue1ID);
		nodeM.save(ue2ID);
		nodeM.save(semestre1ID);
		nodeM.save(semestre2ID);
		nodeM.save(anneeID);
		nodeM.save(parcoursID);
		frmID.setSites(sites);
		nodeM.save(frmID);
		fm.saveWithoutAlteringValidationState(formationID);
		formationID.setUsers(users);

		/*-------------------------------------------*/
		/*--------Formation without credits----------*/
		/*-------------------------------------------*/
		
		Node ueBD = new Node();
		ueBD.setType(Type.UE);
		ueBD.setName("UE Anglais");
		ueBD.setCode("ENSINCU72");
		ueBD.setCredits(0.0f);
		ueBD.setHETD(0.0f);
		ueBD.setHCM(10.0f);
		ueBD.setHTD(10.0f);
		ueBD.setHTP(10.0f);
		ueBD.setHNP(0.0f);
		ueBD.setNbGroupsCM(0);
		ueBD.setNbGroupsTD(0);
		ueBD.setNbGroupsTP(0);

		Node ueBD2 = new Node();
		ueBD2.setType(Type.UE);
		ueBD2.setName("UE Allemand");
		ueBD2.setCode("ENSINCU73");
		ueBD2.setCredits(0.0f);
		ueBD2.setHETD(0.0f);
		ueBD2.setHCM(10.0f);
		ueBD2.setHTD(10.0f);
		ueBD2.setHTP(10.0f);
		ueBD2.setHNP(0.0f);
		ueBD2.setNbGroupsCM(0);
		ueBD2.setNbGroupsTD(0);
		ueBD2.setNbGroupsTP(0);

		Node semestre2IDFED = new Node();
		semestre2IDFED.setType(Type.SE);
		semestre2IDFED.setName("Semestre 2");
		semestre2IDFED.setCode("SE2M2FED");
		semestre2IDFED.setCredits(0.0f);
		semestre2IDFED.setHETD(0.0f);
		semestre2IDFED.setHCM(10.0f);
		semestre2IDFED.setHTD(10.0f);
		semestre2IDFED.setHTP(10.0f);
		semestre2IDFED.setHNP(0.0f);
		semestre2IDFED.setNbGroupsCM(0);
		semestre2IDFED.setNbGroupsTD(0);
		semestre2IDFED.setNbGroupsTP(0);
		semestre2IDFED.setChildren(Arrays.asList(ueBD2));
		
		nodeM.save(ueBD);

	}
}
