package fr.univ.amu.pacome.web;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is; // the one we use is not actually deprecated
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Formation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class FormationsControllerTests {
	private static final String TEST_DATA_PATH = "/fr/univ/amu/pacome/test/data/formation/";

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testListFormations() throws Exception {
		ModelMap model = mockMvc.perform(get("/formations/")).andExpect(status().isOk())
				.andExpect(view().name("formation-list")).andReturn().getModelAndView().getModelMap();

		@SuppressWarnings("unchecked")
		List<Formation> formations = (List<Formation>) model.get("formations");
		assertEquals(formations.size(), 3);
	}

	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testShowFormation() throws Exception {
		mockMvc.perform(get("/formations/1")).andExpect(status().isOk())
				.andExpect(model().attribute("formation", hasProperty("name", is("Informatique"))))
				.andExpect(view().name("formation-details"));
	}

	@Test
	public void testShowFormationNonExistent() throws Exception {
		mockMvc.perform(get("/formations/42000")).andExpect(status().isNotFound());
	}

	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testInitUpdateForm() throws Exception {
		mockMvc.perform(get("/formations/1/edit")).andExpect(status().isOk())
				.andExpect(model().attribute("formation", hasProperty("name", is("Informatique"))))
				.andExpect(view().name("formation-form"));
	}

	@Test
	public void testInitUpdateFormNonExistentFormation() throws Exception {
		mockMvc.perform(get("/formations/42000/edit")).andExpect(status().is(404));
	}

	@Test
	public void testCreateFormationDefaultValues() throws Exception {
		mockMvc.perform(get("/formations/create")).andExpect(status().isOk())
				.andExpect(model().attribute("formation", hasProperty("percentOfCM", is(33.3f))))
				.andExpect(model().attribute("formation", hasProperty("percentOfTD", is(33.3f))))
				.andExpect(model().attribute("formation", hasProperty("percentOfTP", is(33.3f))))
				.andExpect(model().attribute("formation", hasProperty("numberOfHoursPerCredit", is(10f))))
				.andExpect(view().name("formation-form"));
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testDisplayTree() throws Exception {
		mockMvc.perform(get("/formations/1/tree"))
			.andExpect(status().isOk())
			.andExpect(view().name("formation-tree"));
	}

	@Test
	@DatabaseSetup(value = "/fr/univ/amu/pacome/test/data/node/dbunit-node-calcul.xml")
	public void testGetFormationTreeJson() throws Exception{ // Test get tree JSON format
		mockMvc.perform(get("/formations/1/tree.json"))
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}
	
	@Test
	public void testGetFormationTreeJsonNotExist() throws Exception{
		mockMvc.perform(get("/formations/0/tree.json"))
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andExpect(status().isNotFound());
	}
}
