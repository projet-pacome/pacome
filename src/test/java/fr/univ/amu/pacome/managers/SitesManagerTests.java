package fr.univ.amu.pacome.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.tree.Site;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class SitesManagerTests {

	public final String PATH = "/fr/univ/amu/pacome/test/data/site/";

	@Autowired
	private SitesManager sm;
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-site.xml")
	public void testFindOne() {
		Site findOne = sm.findOne(1);
		assertEquals("Luminy", findOne.getName());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-site.xml")
	public void testFindAll() {
		assertNotNull(sm.findAll());
		assertEquals(2, sm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-site.xml")
	public void testSave() {
		Site site = new Site();
		site.setIdSite(3);
		site.setName("Saint-charles");
		sm.save(site);
		assertEquals(3, sm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-site.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-siteUpdate.xml")
	public void testUpdate() {
		Site site = sm.findOne(1);
		site.setName("NewLuminy");
		sm.save(site);
		sm.findAll().size();
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-siteDel.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-site.xml")
	public void testDelete() {
		sm.delete(3);
		assertEquals(2, sm.findAll().size());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-site.xml")
	public void testFindSiteByName(){
		assertEquals(1, sm.findByNameContainingIgnoreCase("lum").size());
	}
}
