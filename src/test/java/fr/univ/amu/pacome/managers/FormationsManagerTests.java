package fr.univ.amu.pacome.managers;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class FormationsManagerTests {
	
	private static final String TEST_DATA_PATH = "/fr/univ/amu/pacome/test/data/formation/";
	@Autowired
	private FormationsManager fm;
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testFindOne() {
		Formation findOne = fm.findOne(1);
		assertEquals("Informatique", findOne.getName());
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testFindAll() {
		assertNotNull(fm.findAll());
		assertEquals(3, fm.findAll().size());
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testSave() {
		Formation formation = new Formation();
		formation.setCMGroupSize(0);
		formation.setTDGroupSize(0);
		formation.setTPGroupSize(0);
		formation.setCode("TEST4");
		formation.setName("Master1");
		formation.setNumberOfHoursPerCredit(10.0f);
		formation.setPercentOfCM(30.0f);
		formation.setPercentOfTD(30.0f);
		formation.setPercentOfTP(40.0f);
		formation.setValidationState(ValidationState.VALID);
		fm.save(formation);
		assertEquals(4, fm.findAll().size());
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = TEST_DATA_PATH + "dbunit-formationUpdate.xml")
	public void testUpdate() {
		Formation formation = fm.findOne(3);
		formation.setCode("TEST5");
		formation.setValidationState(ValidationState.UNKNOWN);
		fm.save(formation);
		fm.findAll().size();
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formationDel.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testDelete() {
		fm.delete(4);
		assertEquals(3, fm.findAll().size());
	}
	
	@Test
	@DatabaseSetup(value = TEST_DATA_PATH + "dbunit-formation.xml")
	public void testFindByNameContainingIgnoreCase(){
		assertEquals(2, fm.findByNameContainingIgnoreCase("%matique%").size());
	}
	
	@Test
	public void testValidatePercentageOfCmTdTp() {
		Formation formation = new Formation();
		formation.setPercentOfCM(33.30f);
		formation.setPercentOfTD(33.86f);
		formation.setPercentOfTP(33.0f);
		formation = fm.percentageValidation(formation);
		assertEquals(33.24f,formation.getPercentOfCM(),0.01);
		assertEquals(33.80f,formation.getPercentOfTD(),0.01);
		assertEquals(32.94f,formation.getPercentOfTP(),0.01);
	}
}