package fr.univ.amu.pacome.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.entities.tree.Type;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class NodeManagerTests {

	public final String PATH = "/fr/univ/amu/pacome/test/data/node/";

	@Autowired
	private NodeManager nm;

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testFindOne() {
		Node findOne = nm.findOne(1);
		assertEquals("ririName", findOne.getName());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testFindAll() {
		assertNotNull(nm.findAll());
		assertEquals(3, nm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testSave() {
		Node node = new Node();
		node.setCode("didi");
		node.setName("didiName");
		node.setType(Type.UEC);
		nm.save(node);
		assertEquals(4, nm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-nodeUpdate.xml")
	public void testUpdate() {
		Node node = nm.findOne(3);
		node.setType(Type.AN);
		nm.save(node);
		node = nm.findOne(3);
		nm.findAll().size();
		assertEquals(Type.AN, node.getType());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-nodeDel.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-node.xml")
	public void testDelete() {
		nm.delete(4);
		assertEquals(3, nm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testFindByNameContainingIgnoreCase() {
		assertEquals(1, nm.findByNameContainingIgnoreCase("ririName").size());

	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testFindByCode() {
		assertEquals(1, nm.findByCode("fifi").size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node.xml")
	public void testfindChildrenByNode() {
		Node node = nm.findOne(1);
		assertEquals(2, node.getChildren().size());
	}

	public void testFindByType() {
		assertEquals(1, nm.findByType(Type.UE).size());
	}

	@Test
	public void testSaveTree() {
		List<Node> listPt = new ArrayList<>();
		List<Node> listAn = new ArrayList<>();
		List<Node> listSe = new ArrayList<>();
		Node frm = new Node();
		Node pt = new Node();
		Node an = new Node();
		Node se = new Node();
		se.setType(Type.SE);
		se.setCode("SE1");
		se.setName("SE1");
		an.setType(Type.AN);
		an.setCode("ISL");
		an.setName("ISL");
		pt.setType(Type.PT);
		pt.setCode("Informatique");
		pt.setName("Informatique");
		frm.setType(Type.FRM);
		frm.setCode("Master");
		frm.setName("Master");
		nm.save(se);
		listSe.add(se);
		an.setChildren(listSe);
		nm.save(an);
		listAn.add(an);
		pt.setChildren(listAn);
		nm.save(pt);
		listPt.add(pt);
		frm.setChildren(listPt);
		nm.save(frm);
	}

	@Test
//	@DatabaseSetup(value = PATH + "dbunit-node-calcul.xml")
	public void testSearchPotentialNewChildren(){
		Node SE1 = new Node();
		SE1.setType(Type.SE);
		SE1.setName("IS");
		SE1.setCode("SEISL01");
		SE1.setCredits(60.f);
		
		Node SE2 = new Node();
		SE2.setType(Type.SE);
		SE2.setName("FISL");
		SE2.setCode("FISL01");
		SE2.setCredits(60.f);
		
		Node PT = new Node();
		PT.setType(Type.PT);
		PT.setName("ISL");
		PT.setCode("ISL01");
		PT.setCredits(60.f);
 		PT.setChildren(Arrays.asList(SE1, SE2));
 		
 		Node root = new Node();
 		root.setType(Type.FRM);
 		root.setName("Master 1 informatique");
 		root.setCode("M1INFO");
 		root.setChildren(Arrays.asList(PT));
		
 		List<Node> potentialChildren = new ArrayList<>();
 		potentialChildren.add(SE1);
 		potentialChildren.add(SE2);
 		
 		nm.save(SE1);
 		nm.save(SE2);
 		nm.save(PT);
 		nm.save(root);
 		
		assertEquals(potentialChildren, nm.searchPotentialNewChildren(PT));
	}
}
