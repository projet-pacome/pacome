package fr.univ.amu.pacome.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Composante;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class ComposantesManagerTests {
	
	public final String PATH = "/fr/univ/amu/pacome/test/data/composante/";
	
	@Autowired
	private ComposantesManager cm;

	@Test
	@DatabaseSetup(value = PATH + "dbunit-composante.xml")
	public void testFindAll() {
		assertNotNull(cm.findAll());
		assertEquals(2, cm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-composante.xml")
	public void testFindOne() {
		Composante findOne = cm.findOne(1);
		assertEquals("UFR Science", findOne.getName());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-composante.xml")
	public void testSave() {
		Composante composante = new Composante();
		composante.setCode("model3");
		composante.setName("URF BIO");
		cm.save(composante);
		assertEquals(3, cm.findAll().size());	
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-composante.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-composanteUpdate.xml")
	public void testUpdate() {
		Composante composante = cm.findOne(2);
		composante.setCode("model2");
		cm.save(composante);
		cm.findAll().size();
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-composanteDel.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-composante.xml")
	public void testDelete() {
		cm.delete(3);
		assertEquals(2, cm.findAll().size());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-composante.xml")
	public void testFindByNameContainingIgnoreCase(){
		assertEquals(1, cm.findByNameContainingIgnoreCase("UFR").size());
	}

}
