package fr.univ.amu.pacome.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Composante;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class UsersManagerTests {

	public final String PATH = "/fr/univ/amu/pacome/test/data/user/";
	public final String PATHFRM = "/fr/univ/amu/pacome/test/data/formation/";

	@Autowired
	private UsersManager um;

	@Autowired
	private FormationsManager fm;

	@Autowired
	private ComposantesManager cm;

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	public void testFindOne() {
		User findOne = um.findOne(1);
		assertEquals("Jean-Luc", findOne.getFirstName());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	public void testFindAll() {
		assertNotNull(fm.findAll());
		assertEquals(3, um.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	public void testSave() {
		User user = new User();
		Formation formation = fm.findOne(1);
		Composante composante = cm.findOne(1);
		Set<Formation> setFormation = new HashSet<Formation>();
		setFormation.add(formation);
		user.setFirstName("Amine");
		user.setLastName("NASSEH");
		user.setFormations(setFormation);
		user.setComposante(composante);
		um.save(user);
		assertEquals("Amine", um.save(user).getFirstName());
		assertEquals(4, um.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-userUpdate.xml")
	public void testUpdate() {
		User user = um.findOne(3);
		user.setFirstName("Rolland2");
		um.save(user);
		assertEquals(3, um.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-userDel.xml")
	public void testDelete() {
		um.delete(3);
		assertEquals(2, um.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	public void testFindByLastNameContainingIgnoreCase (){
		assertEquals(2, um.findByLastNameContainingIgnoreCase("%mass%").size());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-user.xml")
	public void  testFindByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase(){
		assertEquals(2, um.findByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase("mass", "Jean").size());
	}

}
