package fr.univ.amu.pacome.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Nature;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class NaturesManagerTests {

	public final String PATH = "/fr/univ/amu/pacome/test/data/nature/";
	
	@Autowired
	private NaturesManager nm;
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-nature.xml")
	public void testFindAll() {
		assertNotNull(nm.findAll());
		assertEquals(2, nm.findAll().size());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-nature.xml")
	public void testFindOne() {
		Nature findOne = nm.findOne(1);
		assertEquals("Licence", findOne.getName());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-nature.xml")
	public void testSave() {
		Nature nature = new Nature();
		nature.setName("Doctorat");
		nm.save(nature);
		assertEquals(3, nm.findAll().size());	
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-nature.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-natureUpdate.xml")
	public void testUpdate() {
		Nature nature = nm.findOne(2);
		nature.setName("Master Pro");
		nm.save(nature);
		nm.findAll().size();
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-natureDel.xml")
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = PATH + "dbunit-nature.xml")
	public void testDelete() {
		nm.delete(3);
		nm.findAll().size();
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-nature.xml")

	public void findByNameLike(){
		assertEquals(1, nm.findByNameContainingIgnoreCase("%ma%").size());

	}
}
