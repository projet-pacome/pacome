package fr.univ.amu.pacome.test.error_msg;

import static org.junit.Assert.assertEquals;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.tree.Node;
import fr.univ.amu.pacome.managers.NodeManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class ErrorMsgTest {

	public final String PATH = "/fr/univ/amu/pacome/test/data/error_msg/";
	
	Node node;

	@Autowired
	private NodeManager nm;
	
	@Before
	public void setUp() throws Exception {
		node = new Node();
	}

	@After
	public void tearDown() throws Exception {
		node = null;
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-error_msg.xml")
	public void testGetOneErrorMsg() {
		Node node = nm.findOne(2);
		assertEquals("Erreur le fils de PT est une UE", node.getErrorMsg().get(0) );
		assertEquals("fifiName" , node.getName());
	}
	
	@Test
	@DatabaseSetup(value = PATH + "dbunit-error_msg.xml")
	public void testGetTwoErrorMsg() {
		Node node = nm.findOne(2);
		assertEquals("Erreur le fils de PT est une UE", node.getErrorMsg().get(0) );
		assertEquals("Erreur le fils de PT est une EC", node.getErrorMsg().get(1) );
		assertEquals("fifiName" , node.getName());
	}

}
