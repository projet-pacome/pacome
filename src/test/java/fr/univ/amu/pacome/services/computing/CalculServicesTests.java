package fr.univ.amu.pacome.services.computing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;

import javax.transaction.Transactional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.managers.FormationsManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
@PropertySource(value = "classpath:classpath:application-configuration.properties")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
public class CalculServicesTests {

	public final String PATH = "/fr/univ/amu/pacome/test/data/node/";

	@Autowired
	ComputeHETD computeHETD;

	@Autowired
	ComputeNbGroups computeGroupSize;

	@Autowired
	ComputingCore computeCore;

	@Autowired
	private FormationsManager fm;

	@Test
	public void testTauxConversionToHETDTests() throws Exception {
		assertTrue(1.5 == computeHETD.getTauxCmHetd());
		assertTrue(1 == computeHETD.getTauxTdHetd());
	}

	@Test
	@Ignore
	@DatabaseSetup(value = PATH + "dbunit-node-calculBis.xml")
	public void testComputeHetdViaHcmHtdHtp()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation = fm.findOne(1);
		computeCore.estimateFormationHETD(formation);
		assertEquals(123.5, formation.getRoot().getHETDComputed(), 0.1);
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node-calculBis.xml")
	public void testPushEffectif() {
		Formation formation = fm.findOne(1);
		computeGroupSize.compute(formation);
		assertEquals(new Integer(3), formation.getRoot().getNbGroupsCM());
	}

	@Test
	@DatabaseSetup(value = PATH + "dbunit-node-calculBis.xml")
	public void testHETD() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation = fm.findOne(1);
		computeCore.estimateFormationHETD(formation);
		assertEquals(157.1, formation.getRoot().getHETDComputed(), 0.1);
	}

}
