package fr.univ.amu.pacome.services.checker;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import fr.univ.amu.pacome.PacomeApplication;
import fr.univ.amu.pacome.entities.user.Formation;
import fr.univ.amu.pacome.entities.user.ValidationState;
import fr.univ.amu.pacome.managers.FormationsManager;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = "/fr/univ/amu/pacome/test/data/testDBUnit-context.xml")
@SpringBootTest(classes = PacomeApplication.class)
@WebAppConfiguration
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
//		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@PropertySource(value = "classpath:classpath:application-configuration.properties")
@Transactional
public class CheckerServicesTests {
	public final String PATH = "/fr/univ/amu/pacome/test/data/checker/";
	
	@Autowired
	FormationsManager fm;
	
	@Autowired
	Checker checker;

	@Test
	public void StructureTests() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation2 = fm.findOne(2);
		assertFalse(checker.validate(formation2));
		assertTrue(formation2.getValidationState().equals(ValidationState.INVALID));

		Formation formation3 = fm.findOne(1);
		assertTrue(checker.validate(formation3));
		assertTrue(formation3.getValidationState().equals(ValidationState.VALID));
	}
	
	@Test
	public void invalidFormationTests() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		Formation formation1 = fm.findOne(2);
		assertFalse(checker.validate(formation1));
		assertTrue(formation1.getValidationState().equals(ValidationState.INVALID));
	}

}
